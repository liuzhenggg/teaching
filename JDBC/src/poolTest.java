import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet("/pool")
public class poolTest extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            DruidDataSourceTest ds = new DruidDataSourceTest();
            Connection source = ds.getDruidDataSource();

            PreparedStatement prepared = source.prepareStatement("select * from sys_user where user_name = ?");
            prepared.setString(1, req.getParameter("userName"));
            ResultSet rs = prepared.executeQuery();
            while (rs.next()) {
                System.out.println(rs.getString("user_name"));
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
