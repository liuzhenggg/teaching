import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
硬编码: 将连接池的配置信息和 Java 代码耦合在一起
 1、创建 DruidDataSource 连接池对象
 2、设置连接池的配置信息【非必须】
 3、通过连接池获取连接对象
 4.回收连接 【不是关闭 而是用完再放回池子里】
 */
public class DruidDataSourceTest {

//    public Connection getDruidDataSource() throws SQLException {
//
//        DruidDataSource ds = new DruidDataSource();
//        ds.setDriverClassName("com.mysql.jdbc.Driver");
//        ds.setUrl("jdbc:mysql://127.0.0.1:3306/test");
//        ds.setUsername("root");
//        ds.setPassword("root");
//
//        // 非必要的配置
//        ds.setInitialSize(10);
//        ds.setMaxActive(20);
//
//        return ds.getConnection();
//
//    }

    // 使用软编码来
    public Connection getDruidDataSource() throws Exception {
        // 1. 创建 ProPerTies 集合，用于存储外部配置文件的 key 和 v

        // 2. 读取外部配置文件
        InputStream inputStream = DruidDataSourceTest.class.getClassLoader().getResourceAsStream("db.properties");
        Properties props = new Properties();
        props.load(inputStream);

        DataSource dataSource = DruidDataSourceFactory.createDataSource(props);

        return dataSource.getConnection();

    }

}