import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet("/jdbc/*")
public class jdbcTest extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //1 接收用户名和密码
        String requestURI = req.getRequestURI();
        String[] split = requestURI.split("/");
        String methodName = split[split.length - 1];

        if (methodName.equals("query")) {
            query(req, resp);
        }else if (methodName.equals("add")) {
            add(req, resp);
        }

    }

    public void query(HttpServletRequest req, HttpServletResponse resp){
        try {
            //1.注册驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2.获取数据库连接
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
            // 获取参数
            String userName = req.getParameter("userName");
            //3.创建Statement对象
            PreparedStatement preparedStatement = connection.prepareStatement("select * from sys_user where user_name = ?");
            //4.编写SQL语句并执行，获取结果
            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();
            //5.处理结果
            while (resultSet.next()) {
                int uid = resultSet.getInt("uid");
                String username = resultSet.getString("user_name");
                String userPwd = resultSet.getString("user_pwd");
                System.out.println(uid + "\t" + username + "\t" + userPwd);
                // 返回参数给到 ApiFox
                PrintWriter writer = resp.getWriter();
                writer.println(username +" " + userPwd);
            }
            //6.释放资源(先开后关原则)
            resultSet.close();
            preparedStatement.close();
            connection.close();
        }catch (Exception e) {
            System.out.println("数据库连接失败---");
        }
    }


    public void add(HttpServletRequest req, HttpServletResponse resp){
        try {
            //1.注册驱动
            Class.forName("com.mysql.cj.jdbc.Driver");
            //2.获取数据库连接
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "root");
            // 获取参数
            String userName = req.getParameter("userName");
            String userPwd = req.getParameter("userPwd");

            //3.创建Statement对象
            PreparedStatement preparedStatement = connection.prepareStatement("insert into sys_user (user_name,user_pwd)values  (?,?)");
            //4.编写SQL语句并执行，获取结果
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, userPwd);
            preparedStatement.executeUpdate();

            resp.setCharacterEncoding("utf-8"); // 一定要注意这个得设置在返回值前面

            PrintWriter writer = resp.getWriter();
            writer.println("插入数据成功！");

            //6.释放资源(先开后关原则)
            preparedStatement.close();
            connection.close();
        }catch (Exception e) {
            System.out.println("数据库连接失败---");
        }
    }
}
