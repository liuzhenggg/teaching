package weekOne;

//
public class Keywords {

    public static void main(String[] args){
        //转义字符
        // \b作用就是将紧挨着\b的字符向前移动一格
        System.out.print("最后一个字怎么就消失了\b退格");
        //  \n 换行
        System.out.println("\n测试换行");
        //  \t  水平制表
        System.out.println("\t水平制表");
        //  \f 换页 乱码 -- 干掉

        // \r就是将当前位置强制移动到本行开头  如果\r前面有字符则强制删除
        System.out.println("我怎么就消失了呀\r测试回车");
        // 表示一个单引号字符
        System.out.println("这是一个单引号字符\' ");
        // 表示一个双引号字符
        System.out.println("这是一个双引号字符 \"你好\"");
        // 表示一个反斜线
        System.out.println("\\");
        // 130 代表一个八进制数字，转换为十进制数字为88
        int a = '\130';
        System.out.println(a);
    }


}

