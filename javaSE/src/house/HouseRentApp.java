package house;

import house.view.HouseView;

/**
 * 程序执行的入口
 */
public class HouseRentApp {
    public static void main(String[] args) {
        // 创建 HoseView 对象，并显示主菜单，是整个程序的入口
        new HouseView().mainMenu();
        System.out.println("===你退出了房屋出租系统===");

    }
}
