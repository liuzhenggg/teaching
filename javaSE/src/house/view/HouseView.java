package house.view;

import house.model.House;
import house.service.HouseService;

import java.util.Scanner;

/**
 * 1.显示界面
 * 2.接收用户的输入
 * 3.调用 HouseService 完成对房屋信息的各种操作
 */
public class HouseView {

    private boolean loop = true; // 控制显示菜单
    private char key = ' '; // 接收用户选择

    // 设置数组大小为10
    private HouseService houseService = new HouseService(10);


    // 1.编写listHouses()显示房屋列表
    public void listHouses(){
        System.out.println("=============房屋列表=============");
        System.out.println("编号\t\t房主\t\t电话\t\t地址\t\t月租\t\t状态(未出租/已出租)");
        House[] houses = houseService.list(); // 得到房屋信息
        for (int i = 0; i < houses.length; i++) {
            if (houses[i] == null){
                break;
            }
            System.out.println(houses[i]);
        }
        System.out.println("=============房屋列表显示完毕=============");
    }


    // 2.编写addHouse() 接收输入，创建House对象，调用 add 方法
    public void addHouse(){
        System.out.println("=============添加房屋=============");
        Scanner scanner = new Scanner(System.in);

        System.out.println("姓名: ");
        String name = scanner.next();

        System.out.println("电话: ");
        String phone = scanner.next();

        System.out.println("地址: ");
        String address = scanner.next();

        System.out.println("月租: ");
        int rent  = scanner.nextInt();

        System.out.println("状态: ");
        String state = scanner.next();

        // 创建好新的对象加入到 数组中  注意ID 是系统分配的
//        House newHouse = new House(0, name, phone, address, rent, state);
        House newHouse = new House(0, name, phone, address, rent, state);

        if (houseService. add(newHouse)){
            System.out.println("=============添加房屋成功=============");
        }else {
            System.out.println("=============添加房屋失败=============");
        }

    }



    // 3. 删除方法 编写delHouse() 接收输入的id, 调用 service 的 del 方法
    public void delHouse(){
        System.out.println("=============删除房屋信息=============");
        System.out.println("请输入待删除房屋的编号（-1退出）");
        Scanner sc = new Scanner(System.in);
        int delID = sc.nextInt(); // 获取Id
        if (delID == -1){
            System.out.println("=============放弃删除房屋信息=============");
            return;
        }

        System.out.println("请确认是否删除(Y/N),小心选择: ");
        String c;
        for (;;){
            c =  sc.next().toUpperCase();
            if ("Y".equals(c) || "N".equals(c)){
                break;
            }else {
                System.out.println("你输入的参数有误，只能输入y or n 请重新输入");
            }
        }

        if ("Y".equals(c)){ // 这是真的要删除
            if (houseService.del(delID)){
                System.out.println("=============删除房屋信息成功=============");
            }else {
                System.out.println("=============房屋编号不存在,删除失败=============");
            }

        }else {
            System.out.println("=============放弃删除房屋信息=============");
        }
    }

    //4. 查找
    public void findHouse(){
        System.out.println("=============查找房屋信息=============");
        System.out.println("请输入要查找的id: ");
        Scanner scanner = new Scanner(System.in);
        int findId = scanner.nextInt();

        // 调用方法 -- 业务处理 属于业务层的代码
        House house =  houseService.findById(findId);
        if (house != null){
            System.out.println(house);
        }else {
            System.out.println("=============查找房屋信息id不存在=============");
        }
    }

    // 根据id修改房屋信息
    public void update(){
        System.out.println("=============修改房屋信息=============");
        System.out.println("请选择待修改房屋编号(-1表示退出)");
        Scanner scanner = new Scanner(System.in);
        int updateId = scanner.nextInt();
        if (updateId == -1){
            System.out.println("=============你放弃了修改房屋信息=============");
            return;
        }

        // 跟据输入得到 updateId 查找对象
        House house = houseService.findById(updateId);

        if (house == null) {
            System.out.println("=============修改房屋信息编号不存在...=============");
            return;
        }

        System.out.println("姓名("+ house.getName() + "): ");
        String name = scanner.next(); // 这里如果用户直接回车了 用 "" 进行判断再赋值
        house.setName(name); // 因为是引用类型数据 所以这里修改会影响到数组内的那个对象

        // 参数校验
        System.out.println("电话("+ house.getPhone() + "): ");
        String phone = scanner.next();
        house.setPhone(phone);

        System.out.println("地址("+ house.getAddress() + "): ");
        String address = scanner.next();
        house.setAddress(address);

        System.out.println("租金("+ house.getRent() + "): ");
        int rent = scanner.nextInt();
        house.setRent(rent);

        System.out.println("状态("+ house.getState() + "): ");
        String state = scanner.next();
        house.setState(state);

        System.out.println("修改房屋信息成功");
    }

    // 显示主菜单
    public void mainMenu() {
        do {
            System.out.println("\n=============房屋出租系统菜单=============");
            System.out.println("\t\t\t1 新 增 房 源");
            System.out.println("\t\t\t2 查 找 房 屋");
            System.out.println("\t\t\t3 删 除 房 屋 信 息");
            System.out.println("\t\t\t4 修 改 房 屋 信 息");
            System.out.println("\t\t\t5 房 屋 列 表");
            System.out.println("\t\t\t6 退      出");
            System.out.print("请输入你的选择(1-6): ");

            Scanner scanner = new Scanner(System.in);
            key = scanner.next().charAt(0);
            switch (key){
                case '1':
                    addHouse();
                    break;
                case '2':
                    findHouse();
                    break;
                case '3':
                    delHouse();
                    break;
                case '4':
                    update();
                    break;
                case '5':
                    listHouses();
                    break;
                case '6':
                    System.out.println("退 出");
                    loop = false;
                    break;
            }
        }while (loop);

    }


}
