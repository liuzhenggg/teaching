package house.utils;

import java.util.Scanner;

/**
 * 工具类的作用：
 * 处理各种情况的用户输入，并且能够按照程序员的需求，得到用户的控制台输入
 */
public class Utility {

    // 静态属性
    private static Scanner scanner = new Scanner(System.in);

    /**
     * 功能：读取键盘输入的一个菜单选项，值： 1 -- 5 的范围
     */
    public static char readMenuSelection(){
        char c;
        for (;;){
            String str = readKeyBoard(1, false);
            c = str.charAt(0); // 将字符串转换成字符 char
            if (c != '1' && c!= '2' && c!= '3' && c!= '4' && c!= '5' ){
                // 输入参数校验
                System.out.println("您输入的参数有误，请重新输入~~~");
            }else break;
        }
        return c;
    }

    /**
     * 功能：读取键盘输入的一个字符
     * @return 一个字符
     */
    public static char readChar(){
        String str = readKeyBoard(1, false); // 就是一个字符
        return str.charAt(0);
    }


    /**
     * 功能: 读取键盘输入的一个字符，如果直接按回车，则返回指定的默认值
     * @param defaultValue
     * @return
     */
    public static char readChar(char defaultValue){
        String str = readKeyBoard(1, true);
        return (str.length() == 0) ? defaultValue : str.charAt(0);
    }

    /**
     * 功能: 读取键盘输入的指定长度的字符串或默认值，如果直接按回车，返回默认值，否则返回字符串
     * @param limit 限制长度
     * @param defaultValue 指定的默认值
     * @return 指定长度的字符串
     */
    public static String readChar(int limit, String defaultValue){
        String str = readKeyBoard(limit, true);
        return str.equals("") ? defaultValue : str;
    }

    // 字符判断
    public static char readConfirmSelection() {
        System.out.println("请输入你的选择(Y/N)");
        char c;
        for (;;){ // 无限循环
            // 先将小写转换为大写
            String str = readKeyBoard(1, false).toUpperCase();
            c = str.charAt(0);
            if (c == 'Y' || c == 'N'){
                break;
            }else {
                System.out.println("你输入的参数有误，只能输入y or n 请重新输入");
            }
        }
        return c;
    }


    /**
     * 如果输入为空，或者输入大于 Limit 的长度，就会提示重新输入
     */
    private static String readKeyBoard(int limit,boolean blankReturn) {
        // 定义了字符串
        String line = "";

        while (scanner.hasNextInt()) {
            line = scanner.nextLine(); // 读取这一行

            // 如果 line.length = 0 即用户没有输入任何内容，直接回车
            if (line.length() == 0) {
                if (blankReturn) return  line; // 如果 blankReturn 为 true 则可以返回空串
                else continue; // 如果 blankReturn 为false 则不接受空串， 必须输入内容
            }

            if (line.length() < 1 || line.length() > limit) {
                System.out.println("输入长度（不能大于" + limit + "）错误，请重新输入：");
                continue;
            }
            break;
        }

        return line;
    }


    public static String readString(int limit) {
        return readKeyBoard(limit, false);
    }

    /**
     * 获取键盘输入的整数
     * @return 返回处理好的整数
     */
    public static int readInt() {
        int n;
        for (;;){
            String str = readKeyBoard(10, false);// 一个整数 长度<=10
            try {
                n = Integer.parseInt(str);
                break;
            }catch (NumberFormatException e){
                System.out.println("输入的数字有误，请重新输入");
            }
        }
        return n;
    }

}
