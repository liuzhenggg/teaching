package house.service;

import house.model.House;

/**
 * HouseService 类 [业务层]
 * // 定义 House[],保存House 对象
 * 1.响应HouseView 的调用
 * 2.完成对房屋信息的各种操作（CRUD）
 */
public class HouseService {

    private House[] house; // 保存 house 对象

    private int houseNums = 1; // 记录当前有多少个房屋信息
    private int idCounter = 1; // 记录当前的id增长到哪个值

    // 构造器
    public HouseService(int size) {

        // 初始化 这个数组的大小
        house = new House[size];

        // 为了方便测试列表信息，我这里初始化一个House 对象
        house[0] = new House(1, "tom", "123", "团风县", 2000, "未出租");
    }

    // 1. list 方法，返回houses
    public House[] list(){
        return house;
    }


    //2. add 方法，添加新对象，返回Boolean
    public boolean add(House newHouse){
        // 判断是否还可以继续添加（我们暂时不考虑数组扩容问题）
        if (houseNums == house.length){
            System.out.println("数组已满，不能再添加了...");
            return false;
        }

        // 把newHouse对象加入到数组，新增了一个房屋
        house[houseNums++] = newHouse;

        // 我们程序员需要设计一个id 自增长的机制，然后更新 newHouse 的 id
        newHouse.setId(++idCounter);
        return true;
    }

    //3. del 方法，删除一个房屋信息
    public boolean del(int delId){
        // 先找到要删除的访问信息对应的下标
        int index = -1;
        for (int i = 0; i < houseNums; i++) {
            if (delId == house[i].getId()) { // 要删除的访问（id），是数组下标为i的元素
                index = i;
            }
        }

        // 说明没有找到
        if (index == -1){
            return false;
        }

        // 覆盖，将 i+1 覆盖掉i
        for (int i = index; i < houseNums -1; i++) {
            house[i] = house[i + 1];
        }

        // 当把所有存在的访问信息的最后一个 设置为 null
        house[--houseNums] = null;
        return true;

    }

    // 4. 查找
    public House findById(int findId){
        // 遍历数组
        for (int i = 0; i < houseNums; i++) {
            if (findId == house[i].getId()){
                return house[i];
            }
        }
        return null;
    }


}