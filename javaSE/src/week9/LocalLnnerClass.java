package week9;

/**
 * 成员内部类
 */
public class LocalLnnerClass {
    public static void main(String[] args) {
       //  Inner02 inner02 = new Inner02();  外部类无法创建对象  超过了作用域
        // 如果真想 用 可以创建 外部类 然后调用 定义内部类的那个方法
    }
}


class Outer02 { // 外部类

    private int n1 = 100;
    public void m1(){ // 方法
        // 局部内部类是定义在外部类的局部位置的，通常在方法

        // 作用域：仅仅在定义它的方法 或代码块中 --- 就相对于 一个局部变量 -- 只不过这个变量的数据类型是类而已
        class Inner02 { // 局部内部类 ---
            public void test() {
                System.out.println("n1 = " + n1);
            }

        }

        // 5 访问 : 只能在这个 方法内部
        Inner02 inner02 = new Inner02();
        inner02.test();

    }
}