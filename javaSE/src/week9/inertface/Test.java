package week9.inertface;

public class Test {
    public static void main(String[] args) {
        Camera camera = new Camera();
        Phone phone = new Phone();

        Computer computer = new Computer();
        computer.work(camera); // 将相机接入电脑

        System.out.println("====================");
        computer.work(phone); // 将手机 接入电脑
    }
}
