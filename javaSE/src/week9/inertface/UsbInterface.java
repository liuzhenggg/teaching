package week9.inertface;

/**
 * 注意 这里 的关键字 interface
 */
public interface UsbInterface {
    // 规定接口的相关方法 --- 我老师规定的。即规范
    public void start();

    // 抽象方法的集合
    public void stop();
}
