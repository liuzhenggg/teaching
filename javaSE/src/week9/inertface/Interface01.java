package week9.inertface;


public interface Interface01 {

    // 写属性
    public int n1 = 10;

    // abstract 写不写都一样
    public abstract void test();

    // jdk 8.0 后 接口类 可以有静态方法, 默认方法, 但是需要 default 关键字
    default public void ok(){
        System.out.println("ok....");
    }

    // 静态
    public static void  cry(){
        System.out.println("可以有静态方法····");
    }

    // 私有方法


}
