package week9.inertface;

// 这个是电脑
public class Computer {

    // 编写一个方法
    /*
        重要 --- 这里 就是电脑提供的接口
        你想 插入电脑 ？ 我把标准提供出来了 你去是适配这个标准 就可以接入了

        接口核心要点 ： 设计者定义一套规定规范标准， 给使用者 实现 使代码更加规范
     */
    public void work(UsbInterface usbInterface) {
        usbInterface.start();
        // 这中间还可以跟据业务插入很多其他代码
        usbInterface.stop();
    }

}
