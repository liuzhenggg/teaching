package week9.inertface;

// 为了适配 所以 要实现接口 【接受统一的标准】
public class Phone implements UsbInterface {

    @Override
    public void start() {
        System.out.println("手机开始工作....");
    }

    @Override
    public void stop() {
        System.out.println("手机停止工作...");
    }
}
