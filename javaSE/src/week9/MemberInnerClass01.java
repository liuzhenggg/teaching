package week9;

/**
 * 成员内部类
 */
public class MemberInnerClass01 {
    public static void main(String[] args) {

        //先 创建 外部类的对象
        Outer08 outer08 = new Outer08();
        // 相当于把 new Inner08 当做是 outer08成员
        // 这就是一个语法 不必纠结
        Outer08.inner0 inner0 = outer08.new inner0();
        inner0.test();
    }

}

class Outer08 {

    class  inner0{
        public void test() {
            System.out.println("1111");
        }
    }
}