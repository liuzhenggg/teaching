package week9;

/**
 * 匿名内部类
 */
public class AnonymouslnnerClass01 {
    public static void main(String[] args) {
        Outer01 outer01 = new Outer01();
        outer01.method();
    }
}

class Outer01 { // 外部类
    private int n1 = 10;
    public void method(){
        // 1. 基于接口的 匿名内部类
        // 2. 需求：想使用IA 接口，并创建对象
        // 3. 传统方式；是写一个类，实现该接口，并创建对象
            // 但是 如果 我这个类只会使用一次，以后不会再使用了
            // 所以 我就搞一个匿名内部类 为了 简化操作可以使用匿名内部类来简化开发
        IA tiger = new IA() {
            @Override
            public void cry() {
                System.out.println("老虎在叫唤。。。");
            }
        };

        tiger.cry(); // 不用创建类 一样用
        // 4:
        // tiger 的编译类型 是 IA  【编译类型 看等号左边】
        // tiger 的运行类型 是 匿名内部类 【运行类型 看等号右边】
            // 看类名
        // 5. 匿名内部类 创建了 tiger 对象后 返回对应的地址 给到 tiger 类信息就没有了
        System.out.println("tiger 的运行类型 " + tiger.getClass());

        // 基于抽象类的匿名 内部类的使用  【注意 -- 匿名类 不只是可以 匿名 抽象类 和接口 普通类也可以 只不过相当于建了其子类然后进行了重写】

        // 匿名对象 --- 只用一次 【一般为了追求效率 我会这样写 但是如果注释没写好会坑新手】
        new Father("匿名对象"){
            @Override
            public void test() {
                System.out.println("重写了···");
            }
        }.test();

    }
}

class Father {
    public Father(String name) { }
    public void test(){
        System.out.println("没重写前");
    }
}

interface IA {
    public void cry();
}