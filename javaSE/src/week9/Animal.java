package week9;

public class Animal {

    /*
        思考： 这里eat 你不知道里面要怎么实现
        即： 父类方法不确定
        ======》 就可以考虑将该方法设计为抽象类 Abstract
        ======》 所谓抽象方法 就是没有实现的方法
        ======》 所谓没有实现就是指 没有方法体
     */
    public void eat(){
        System.out.println("动物吃东西~~~~");
    }
}
