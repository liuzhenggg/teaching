package week9;

public abstract class Teacher {
    public abstract void work(); // 老师在上课 但是不知道是哪个老师 所以也就不知道是 上什么课
}


class MethTeacher extends Teacher{

    @Override  // 这个的注解
    // 注解有三种
        // 预定义注解 --- 可以理解为 JDK (框架) 自带的
        // 元注解 --- 注解的注解
        // 自定义注解 --- 开发者自己的注解
    // 注解 是给人看的 也是给编译器看的 告知编译器要做什么事情
    public void work() {

    }
}