package week9.vs;

public class ExtendsVsInterface {

    public static void main(String[] args) {
        LiteMonkey wk = new LiteMonkey("悟空");
        wk.climbing();
        wk.swimming();
    }
}

class LiteMonkey extends Monkey implements FishAble{

    public LiteMonkey(String name) {
        super(name);
    }

    @Override
    public void swimming() {
        System.out.println("通过学习： " + getName() + " 学会了游泳");
    }
}

class Monkey {
    private String name;

    public Monkey(String name) {
        this.name = name;

    }

    public void climbing() {
        System.out.println(name + " 会爬树");
    }

    public String getName() {
        return name;
    }
}

// 接口是对 java内 的单继承的 机制进行补充
interface FishAble {
    void swimming();
}