package week9.poly_;

public abstract class Animal {
    public String name;

    public abstract String getName();

    public abstract void setName();
}
 