package week9.poly_;

public class Dog extends Animal {
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName() { }

    public Dog(String name) {
        super();
        this.name = name;
    }
}
