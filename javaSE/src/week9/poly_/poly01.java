package week9.poly_;

/**
 * 多态
 */
public class poly01 {

    public static void main(String[] args) {
        Master tom = new Master("汤姆");
        Dog dog = new Dog("斯派克");
        Bone bone = new Bone("大骨棒子");
        tom.feed(dog,bone);


        // 匿名内部类的写法
        tom.feed(new Animal() {
            private String name = "小鸟";
            @Override
            public String getName() {
                return name;
            }

            @Override
            public void setName() {}

        }, new Food() {
            private String name = "小虫";
            @Override
            public String getName() {
                return name;
            }

            @Override
            public void setName() {}

        });
    }

}
