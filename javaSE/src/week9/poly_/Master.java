package week9.poly_;

public class Master {
    private String name;

    public Master(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // 核心 --- 主人给小狗 喂食 骨头
    public void feed1(Dog dog , Bone bone){
        System.out.println("主人 " + name + " 给 " + dog.getName() + " 吃 " + bone.getName());
    }

    // 主人给 小猫喂 黄花鱼
    public void feed1(Cat cat,Fish fish) {
        System.out.println(" 主人 " + name + " 给 " + cat.getName() + " 吃 " + fish.getName());
    }

    // 但是 动物园里面 有100种动物 而 100种动物 吃的事物又不一样 怎么搞 ？？？


    // 使用多态 解决 前面的代码 就都可以删掉了
    public void feed(Animal animal, Food food){
        System.out.println(" 主人 " + name + " 给 " + animal.getName() + " 吃 " + food.getName());    }
}
