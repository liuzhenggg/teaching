package week9.poly_;

public abstract class Food {
    public String name;

    public abstract String getName();

    public abstract void setName();
}
