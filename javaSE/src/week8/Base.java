package week8;

public class Base { // 父类
    public int n1 = 100;
    protected int n2 = 200;
    int n3 = 300;
    private int n4 = 400;

    public Base() { // 父类的无参构造器
        System.out.println("base()....");
    }

    public static void test() {  // 被 static 修饰的方法 就是静态 方法了 可以被继承 但是和重写就已经无关了
        System.out.println("fff");
    }

    public void test100(){
        System.out.println("test100");
    }

    private void test200(){
        System.out.println("test200");
    }

    public int getN4(){
        return this.n4;
    }
}
