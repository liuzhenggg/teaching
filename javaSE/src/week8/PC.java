package week8;

public class PC extends Computer{

    private String brand;

    // 父类的属性 通过父类的构造器 进行初始化
    public PC(String cpu, int memory, int disk, String brand) {
        super(cpu, memory, disk);
        this.brand = brand;
    }

    // 子类的属性 通过子类的构造器完成初始化
    public PC(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void printInfo(){
        System.out.println("PC信息=");
        System.out.println(getDetails() + "brand=" +brand);
    }
}
