package week8;

/**
 * 继承的本质
 */
public class ExtendsTheory {
    public static void main(String[] args) {

        // 访问规则：按照查找关系来返回信息
        /*
            1.首先看子类是否有该类属性
            2.如果子类有这个属性，并且可以访问，则返回信息
            3.如果子类没有这个属性，就看父类有没有这个属性
            4.如果父类也没有就按照 3 的规则，继续找上级父类，直到object
         */
        Son son = new Son();// 内存布局
        System.out.println(son.name);
    }
}

class GrandPa{ //爷爷类
    String name = "大头爷爷";
    String hobby = "喝茶";

    public static void  test() {
        System.out.println("父类的静态方法");
    }

}

class Father extends GrandPa {
    String name = "大头爸爸";
    int age = 35;
}

class Son extends Father{
    String name = "大头儿子";
}
