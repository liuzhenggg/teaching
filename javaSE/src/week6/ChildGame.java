package week6;

public class ChildGame {

    public static void main(String[] args) {
        int count = 0;
        Child c1 = new Child("鲤鱼精");
        c1.join();
        count++;

        c1.count++;

        Child c2 = new Child("烤面筋");
        c2.join();
        count++;

        System.out.println("一共有 " + count + " 个小孩加入了游戏~~~");

        // 这里 用 三个对象分别取访问 count
    }

}
