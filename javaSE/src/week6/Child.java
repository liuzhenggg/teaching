package week6;

public class Child {
    private String name;

    // 最大的特点就是 被所有的对象共享
    public static int count = 0;

    public Child(String name) {
        this.name = name;
    }

    public void join(){
        System.out.println(name + " 加入了游戏..");
    }
}
