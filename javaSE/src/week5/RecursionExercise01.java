package week5;

/*
    递归问题 -- 猴子吃桃
 */
public class RecursionExercise01 {


    /*
        思路分析 逆推
        1. day = 10 时 有 1 个桃子
        2. day = 9 时 有 （day10+1） * 2 = 4
        3. day = 8 时 有 （day10+1） * 2 = 4
        4. 规律就是 前一天的桃子 = （后一天的桃子 + 1）* 2
        5. 递归

     */

    public  int peach(int day) {
        if (day == 10) {
            return 1;
        } else if (day >= 1 && day <= 9) {
            return (peach(day + 1) + 1 * 2);
        }else {
            System.out.println("day 在 1-10");
            return -1;
        }
    }
}
