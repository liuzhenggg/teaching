package week5;

/*
    方法的传参机制
 */
public class MethodParameter {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;

        AA aa = new AA();
        aa.swap(a, b);
        System.out.println("a= " + a + "b=" + b);
    }
}

class AA {
    public void swap(int a, int b) {
        System.out.println("a和b交换前的值 " + a + "\tb=" + b);

        // 完成交换
        int temp = a;
        a = b;
        b = temp;
        System.out.println("a和b交换后的值 " + a + "\tb=" + b);
    }

}