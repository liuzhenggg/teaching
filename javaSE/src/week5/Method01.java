package week5;
/*
    方法快速入门
 */
public class Method01 {
    public static void main(String[] args) {
        // 方法使用
        // 1. 方法写好后你 如果不去调用（使用）不会输出
        // 2.先创建对象，然后调用方法即可 ---- main 方法是程序的入口
        Person p1 = new Person();
        p1.speak();
        p1.cal01();
        p1.cal02(5); // 调用方法 --- 传入5
        p1.cal02(10); // 调用方法 --- 传入10
        int sum = p1.getSum(10, 17);
        System.out.println(sum);



    }
}


