package week5;

public class Person {

    String name;
    int age;

    /*
        方法 （成员方法）
        1. public 表示方法是公开的
        2. void : 表示方法没有返回值
        3. speak() 是方法名 （）形参列表
        4. {}方法体， 可以写我们要指向的代码
        5. System.out.println("我是最棒的！"); 表示我们的方法就是输出一句话
     */
    public void speak() {
        System.out.println("我是最棒的！");
    }


    // 添加 cal01 成员方法 计算 1 + 10000 的结果
    public void cal01() {
        int res = 0;
        for (int i = 1; i <=1000 ; i++) {
            res += i;
        }
        System.out.println("cal01 计算结果=" + res);
   }

   // (int n) 形参列表 表示当前有一个形参 n ; 可以接收用户输入
    public void cal02(int n) {
        int res = 0;
        // 循环完成
        for (int i = 1; i <=n ; i++) {
            res += i;
        }
        System.out.println("cal02 方法计算结果=" + res);
    }


    /*
        形参列表：(int number1 , int number2)
        两个形参 可以接收用户输入的两个数
     */
    public int getSum(int number1 , int number2) {
        int res = 0;
        res = number1 + number2;
        return res;
    }


}
