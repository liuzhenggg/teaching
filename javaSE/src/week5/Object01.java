package week5;
/*
    类与对象的引入
 */
public class Object01 {

    /*
        刘老师养了两只猫，一只名字叫大橘儿，今年50岁，橘色。
        还有一只叫小白，今年3岁，白色。
        请编写一个程序，当用户输入小猫的名字时，就显示该猫的名字，年龄，颜色。
        如果用户输入的小猫名错误，则提示，刘老师没有这只猫。
     */

    public static void main(String[] args) {
        // 单独用变量来解决 ----》不利于数据的管理 ----》你把一只猫的信息拆解了！！！

        // 第一只猫的信息
        String cat1Name = "大橘儿";
        int cat1Age = 50;
        String cat1Color = "橘色";
        // 如果不止这些呢？ 体重？ 爱好（吃） ~~~ 麻烦

        // 第二只猫的信息
        String cat2Name = "小白";
        int cat2Age = 100;
        String cat2Color = "白色";

        // 那数组呢？-----> 也不行，（1）数据类型体现不出来 （2）只能通过下标获取数据---关系不明确 （3）体现不出来猫的行为
//        String[] cat1 = {""};
//        String[] cat2 = {""};

        Cat cat1 = new Cat();
        cat1.age = 50;
        cat1.name = "大橘儿";
        cat1.color = "橘色";

        Cat cat2 = new Cat();
        cat2.age = 3;
        cat2.name = "小白";
        cat2.color = "白色";

        // 用对象去表示
        System.out.println(cat1.age + cat1.name);

    }


}

class Cat {
    // 属性
    String name;
    int age;
    String color;
}