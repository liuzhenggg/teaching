package week5;

public class PropertiesDetail {

    public static void main(String[] args) {
        // 创建Person 对象
        // p1 是对象名（对象的引用）
        // new Person（） 创建的对象空间（数据）才是真正的对象
        Person person = new Person();

        // 输出这个人的信息
        System.out.println(person.age);

        Person p1 = new Person();
        p1.age = 10;
        p1.name = "小明";

        Person p2 = p1;

        // 改 age
        // p1.age = 50;
        System.out.println(p2.age);

        Person a = new Person();
        a.age = 10;
        a.name = "小明";
        Person b;
        b = a;
        System.out.println(b.name);
        b.age = 200;
        b = null;
        System.out.println(a.age);
        System.out.println(b.age);


    }
}
