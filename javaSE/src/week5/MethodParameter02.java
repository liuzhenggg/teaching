package week5;

import java.util.Arrays;

/*
    引用数据类型传递参数
 */
public class MethodParameter02 {

    public static void main(String[] args) {
        B b = new B();
        int[] arr = {1, 2, 3};
        b.test100(arr);
        System.out.println(Arrays.toString(arr));

        Per p = new Per();
        p.name = "jake";
        p.age = 10;

        b.test200(p);
        System.out.println(p.age + p.name);
    }
    
}

class Per{
    String name;
    int age = 10;
}

class  B {
    public void test200(Per p){
        p.age = 100;
        // 把这里修改为 null
        // p = null
        // 思考2
//        p = new Per();
//        p.name = "tom";
//        p.age = 99;
    }

    public void test100(int[] arr){
        arr[0] = 200;
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i] + "\t");
        }
        System.out.println();
    }
}
