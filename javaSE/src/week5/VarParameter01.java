package week5;

/*
    可变参数
 */
public class VarParameter01 {

    public int sum(int... nums) {
        // 打印长度
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            res += nums[i];
        }
        return res;
    }


}
