package week5;
/*
    递归
 */
public class Recursion01 {

    // 输出什么 ？？
    public  int factorial(int n) {
        if (n == 1) {
            return 1;
        }else {
            return factorial(n - 1) * n ;
        }
    }


    // 输出什么 ？？
    public  void test1(int n) {
        if (n > 2) {
            test1(n-1);
        }
        System.out.println("n=" + n);
    }

}
