package week5;

/*
    方法的入门
 */
public class Method02 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 3, 5, 9, 8, 7, 5};
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }

        // 如果这里需要再输出一次呢？
        // 这里再需要输出一次？
        // 重复输出----
    }
}
