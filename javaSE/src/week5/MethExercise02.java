package week5;

public class MethExercise02 {
    public static void main(String[] args) {
        PerS p = new PerS();
        p.name = "xx";
        p.age = 100;
        MyTools1 tools1 = new MyTools1();
        PerS perS = tools1.copyPerson(p);

        // 到此 p 和 perS 是 PerS 的对象，但是是两个独立的对象，但是属性相同

    }
}

class PerS {
    String name;
    int age;

}

class MyTools1{


    public  PerS copyPerson(PerS p){
        // 创建一个新的对象
        PerS p2 = new PerS();
        p2.name = p.name;
        p2.age = p.age;
        return p2;
    }


}