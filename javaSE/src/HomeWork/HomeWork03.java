package HomeWork;

import java.util.Scanner;

/**
 * 控制结构，作业
 */
public class HomeWork03 {
    /*
      1.编程实现如下功能
        某人有 100,000元，每经过一次路口就需要交费，规则如下：
        1）当现金 > 50000时，每次交5%
        2）当现金 <= 50000，每次交1000
        编程计算该人可以经过多少次路口，要求：使用 while break 方式完成

      2.键盘输入一个整数，属于哪个范围：大于0；小于0；等于0

      3.键盘输入一个年份，判断这个年份是否为闰年 ---> 61面 例题

      4.输出1--100之间的不能被5整除的数，每5个一行

      5.求1+（1+2）+（1+2+3）+（1+2+3+4）+ .... +(1+2+3+...+100) 的结果

      6.从键盘输入一个整数，控制台输出整数对应层数的金字塔

      7.P94 第四章课后习题的四道操作题

      备注：统一格式：HomeWork01 --- HomeWork10

     */

    public static void main(String[] args) {
        work04();
    }

    // 穷举法
    public static void work01(){
        int num1, num2,num3;
        Scanner ss = new Scanner(System.in);
        System.out.println("请输入第一个数：");
        num1 = ss.nextInt();
        System.out.println("请输入第二个数：");
        num2 = ss.nextInt();
        System.out.println("请输入第三个数：");
        num3 = ss.nextInt();
        if (num1 <= num2 && num2 <= num3) {
            System.out.println("从小到大排列为:" + num1 + num2 + num3);
        }
        if (num1<= num2&&num1<=num3&&num3<= num2) {
            System.out.println("从小到大排列为:" + num1 + num3 + num2);
        }
        if (num3<= num2&&num2<=num1&&num3<= num1) {
            System.out.println("从小到大排列为:" + num3 + num2 + num1);
        }
        if (num3 <= num2&&num3<=num1&&num1<= num2) {
            System.out.println("从小到大排列为:" + num3 + num1 + num2);
        }
        if (num2<= num3&&num3<=num1&&num2<= num1) {
            System.out.println("从小到大排列为:" + num2 + num3 + num1);
        }
        if (num2<= num1&&num2<=num3&&num1<= num3){
            System.out.println("从小到大排列为:" + num2+num1+num3);
        }
    }

    public static void work02(){

    }

    /*
        课本练习题 --- 鸡兔同笼
     */
    public static void work03(){
        int chicken, rabbit; // 鸡和兔的数量
        int head = 35; // 头的数量
        int foot = 90; // 脚的数量

        // 穷举法
        for (chicken = 1; chicken <= head; chicken++) {
            rabbit = head - chicken;

            // 满足条件
            if (rabbit * 4 + chicken * 2 == foot) {
                System.out.println("兔子 " + rabbit );
                System.out.println("鸡 " + chicken);
            }

        }

    }


    /*
        课本练习题 --- 质素
     */
    public static void work04() {

        for (int i = 3;i<= 100;i++){//1既不是质数也不是和数，所以从2开始
            boolean k = true;

            // 所有的数都能被 1 整除 所以从 2开始
            for (int n = 2; n < i; n++) {
                // 如果不是质数，说明它可以被整除 -- 直接跳出
                if (i % n == 0) {
                    k = false;
                    break;
                }
            }

            if(k){
                System.out.print(i + " ");
            }

        }

    }


    /*
        1.编程实现如下功能
                某人有 100,000元，每经过一次路口就需要交费，规则如下：
                1）当现金 > 50000时，每次交5%
                2）当现金 <= 50000，每次交1000
                编程计算该人可以经过多少次路口，要求：使用 while break 方式完成
            思路分析：
                1.定义 double money 保存 100 000
                2.三种情况
                    money > 50000
                    money >= 1000 且小于等于 50000
                    money < 1000
                3.使用多分支 if -elseif-else
                4.while + break[ 钱小于 1000 ] 同时使用变量 count 保存通过次数
    */
    public static void work05(){
        double money = 100000;
        int count = 0;
        while (true){
            if (money > 50000){
                money = money * 0.95;
            } else if (money >= 1000 && money <= 50000) {
                money = money - 1000;
            }else {
                break;
            }
            count++;
        }
        System.out.println("这个人一共经过了 " + count + " 次路口" + " 还剩下 " + money);
    }

    /*
       输出1--100之间的不能被5整除的数，每5个一行
     */
    public static void work06(){
        int count = 0;

        for (int i = 1; i <=100 ; i++) {

            if (i % 5 !=0 ){
                count++;
                System.out.print(i + " ");
            }

            if (count % 5 == 0) {
                // 输出换行
                System.out.println();
            }

        }
    }


    /*
        双层循环 1+（1+2）+（1+2+3）+（1+2+3+4）
     */
    public static void work07(){
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            // 内层循环，每一项 都是一次内层循环
            for (int j = 1; j <= i; j++) {
                sum = sum + j;
            }
        }
        System.out.println(sum);

    }

}
