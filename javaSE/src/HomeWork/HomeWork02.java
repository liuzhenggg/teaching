package HomeWork;

import java.util.Scanner;

/**
 * 三章运算符的作业目前没下达
 * 明天下达：
 * 2班 ：周三交
 * 3班 ：周三交
 * 6班 ：周四交
 *
 * 答案以注释的形式呈现，此次作业交java源文件即可
 */
public class HomeWork02 {
    /*
    1. 计算下列表达式的结果,以文字分析阐述结果由来
    10/3;
    10/5;
    10%2;
    -10.5%3 ---->  -1.5  a%b --> a - a / b * b  ---> -10.5 - -10.5/3 *3  ---> -10.5 + 9

    2.思考下面代码j的结果是多少,以文字分析阐述结果由来
    int i = 10;
    int j = ++i+i
    System.out.println(j);  ---> 22

    3.多选题，以下赋值语句正确的是() 并指出说明所有赋值错误选项的原因
      A int num1 = int "10";    --> 双引号为字符串
      B int num2 = 18.0;        ---> 精度不够
      C double num3 = 30;       ----> 这个没问题
      D double num4 = 8;        ----> 这个没问题
      E int i = 49; char ch = i +1;  ---> 这个不行 需要强转
      F byte b = 19; short s = b + 2;  ---> 这个不行 需要强转

    4.思考以下题目的输出结果，并说明运算过程
      题目一：
        int x = 3;
        int y = 3;
        if (x++==4 & ++y==4){
            x = 10;
        }
        System.out.println("x =? "+ x + "y =?"+y); ---> x=4  y=4

       题目二:
        int x = 3;
        int y = 3;
        if (x++==4 && ++y==4){
               x = 10;
        }
        System.out.println("x =? "+ x + "y =?"+y);  ---> x=4  y=3

       题目三:
         int x = 3;
         int y = 3;
         if (x++==3 || ++y==3){
              x = 10;
         }
         System.out.println("x =? "+ x + "y =?"+y);  ---> x=10  y=3

       5.课本3.8操作题

     */

    public static void main(String[] args) {

    }

    /*
       将给定小写字母转换成大写字母
     */
    public static void  homeWork01(){
        // 方法一
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入小写字母");
        char c = scanner.next().charAt(0);
        char c1 = Character.toUpperCase(c);
        System.out.println("小写字母: "+c+"大写字母: "+c1);

        // 方法二
        // 小写字母的ASCII码范围是97到122
        // 而大写字母的ASCII码范围是65到90
        char c2 = scanner.next().charAt(0);;
        char upperCaseC = (char) (c2 - 32);
        System.out.println(upperCaseC);

    }

    /*
        将给定的华氏温度 转换成摄氏温度
     */
    public static void  homeWork02(){
        // 1.定义一个变量 保存摄氏温度
        double sheShi;
        // 2. 给一个华氏温度
        double huaShi = 234.1;
        sheShi = 5 * (huaShi - 32) / 9;
        System.out.println("华氏温度 "+huaShi+ "转化为摄氏温度为 "+ sheShi);
    }

    /*
        任意给定一个三位数，请输出该数的个位、十位和百位
     */
    public static void homeWork03(){
        int number = 123; // 示例数字
        int ones = number % 10; // 个位
        int tens = (number / 10) % 10; // 十位
        int hundreds = number / 100; // 百位

        System.out.println("个位数是: " + ones);
        System.out.println("十位数是: " + tens);
        System.out.println("百位数是: " + hundreds);
    }


}
