package HomeWork;

import java.util.Arrays;
import java.util.Scanner;

/**
 * 数组 -- 作业
 */
public class HomeWork04 {
    /*
    作业：
        1.以知有个升序数组，要求插入一个元素后，该数组的顺序依然是升序，
        比如：[10,11,18,60,99],添加 30 后 [10,11,18,30,60,99] (提示：扩容+定位)

        2.键盘输入8个整数（要是无序的）,保存到数组 并倒序打印，并求平均值，打印最大值的坐标

        3.写出冒泡排序代码 --- 要求加上注释 阐述内外层循环的作用分别是什么 --- 不写代码 --- 直接拿出 PPT

        第五章课后练习 操作题

        统一格式：HomeWork01 --- HomeWork06
     */
    public static void main(String[] args) {
        homeWork04();

    }

    /*

        以知有个升序数组，要求插入一个元素后，该数组的顺序依然是升序，
        比如：[10,11,18,60,99],添加 30 后 [10,11,18,30,60,99] (提示：扩容+定位)
        思路：
            1.我们先确定 添加数应该插入到哪个索引
            2.然后扩容
     */
    public static void homeWork01() {
        int[] arr = {10, 12, 45, 90};
        int insertNum = 22;
        int index = -1; // 因为下标不可能为 -1
        // 定位
        // 遍历 arr 数组，如果发现 insertNum <= arr[i] 说明 i 就是要插入的位置
        // 使用 index 保留 index = 1
        for (int i = 0; i < arr.length; i++) {
            if (insertNum <= arr[i]) {
                index = i;
                break; // 找到位置就退出
            }
        }
        // 没有找到位置
        if (index == -1) {
            index = arr.length;
        }
        // 扩容
        int[] arrNew = new int[arr.length + 1];
        int j = 0;
        for (int i = 0; i < arrNew.length; i++) {
            if (i != index) { // 如果位置没到 就可以进行拷贝
                arrNew[i] = arr[j];
                j++;
            } else {  // i 这个位置就是要插入的数
                arrNew[i] = insertNum;
            }
        }
        // 修改地址指向
        arr = arrNew;
        System.out.println("====插入后，arr数组的元素情况====");
        System.out.println(Arrays.toString(arr));
    }

    // 2.键盘输入8个整数（要是无序的）,保存到数组 并倒序打印，并求平均值，打印最大值的坐标
    public static void homeWork02() {
        int[] arr = new int[8];
        Scanner scanner = new Scanner(System.in);
        // 循环输入 8个 值
        for (int i = 0; i < 8; i++) {
            System.out.println("我输入的第 " + (i + 1) + "个数为：");
            arr[i] = scanner.nextInt();
        }
        System.out.println("数组为：" + Arrays.toString(arr));

        // 倒序打印
        for (int i = arr.length - 1; i >= 0; i--) {
            System.out.print(arr[i] + "\t");

        }

        // 平均值 和 最大值的坐标
        double sum = 0;
        int max = arr[0];
        int maxIndex = 0;
        for (int i = 1; i < arr.length; i++) {
            if (max < arr[i]) { // 说明 max 不是最大值、
                max = arr[i]; // 换位置直到求出最大值
                maxIndex = i;
            }
            sum += arr[i];
        }
        System.out.println("最大值是： " + max + " 坐标是: " + maxIndex);
        System.out.println("平均值是： " + sum / arr.length);

    }

    // 冒泡排序 进行排序
    public static void homeWork03() {

        double[] arr = new double[10];
        Scanner scanner = new Scanner(System.in);
        // 循环输入10个积分
        for (int i = 0; i < 10; i++) {
            System.out.println("我输入的第 " + (i + 1) + "个积分为：");
            arr[i] = scanner.nextDouble();
        }
        System.out.println("数组为：" + Arrays.toString(arr));

        double temp = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            // array.length-1 - i 是怎么来的 ？
            for (int j = 0; j < arr.length - 1 - i; j++) {
                // 如果前面的数 > 后面的数，就交换
                if (arr[j] > arr[j + 1]) {
                    // 交换  ---> 需要一个中间变量
                    // 先把 i 给temp
                    // 再把 j+1 给 j
                    // 再把 temp 给到 j+1
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }


    // 删除一维数组中重复的数据 --- 这个题不用 set 集合去做的话很难
    public static void homeWork04() {
        int[] arr = {12, 22, 11, 22, 135, 123, 4, 53, 3123,4};
        int[] Arr = new int[arr.length];
        //外层循环 -- 循环每一个元素
        for (int i = 0; i < arr.length ; i++) {
            // 内层循环 进行元素间的比较
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    Arr[j] = 1;
                }
            }
        }
        int count = 0;
        for (int i = 0; i < Arr.length; i++) {
            if (Arr[i] == 0) { // 为 0 就是默认值
                count++; // 统计不重复的元素的个数
            }
        }
        System.out.println("不重复元素的个数" + count);
        int[] newArr = new int[count]; // 新开一个数组--装不重复的元素

        int index = 0;
        for (int i = 0; i < Arr.length; i++) {
            if (Arr[i] == 0) {
                newArr[index++] = arr[i]; // 把不重复的元素赋值给 新数组
            }
        }
        System.out.println("去除掉重复元素后的数组:");
        for (int i = 0; i < newArr.length; i++) {
            System.out.print(newArr[i] + " ");
        }

    }

    // 矩阵相加
    public static void homeWork05(){

        int[][] matrixA = {
                {1, 2, 3},
                {4, 5, 6}
        };

        int[][] matrixB = {
                {7, 8, 9},
                {10, 11, 12}
        };

        int[][] result = new int[matrixA.length][matrixA[0].length];
        for (int i = 0; i < matrixA.length; i++) {
            for (int j = 0; j < matrixA[0].length; j++) {
                result[i][j] = matrixA[i][j] + matrixB[i][j];
            }
        }

        // 打印结果
        // 外层循环 -- 数组的个数
        for (int i = 0; i < result.length ; i++) {
            // 内层，循环打印每个数组
            for (int j = 0; j < result[i].length; j++) {
                System.out.print(result[i][j] + "\t");
            }
            System.out.println();
        }
    }


}
