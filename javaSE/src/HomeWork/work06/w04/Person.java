package HomeWork.work06.w04;

import week9.inertface.Phone;

/**
 * 有一个交通工具接口类 Vehicles, 有work接口
 * 有 Horse 类和 Boat类分别实现 Vehicles
 * 创建交通工具工厂类，有两个方法分别获得交通工具 Horse 和 Boat
 * 有Person类，有name 和Vehicles 属性，在构造器中为两个属性赋值
 * 实例化Person 对象“唐僧”，要求一般情况下使用 Horse 作为交通工具，遇到大河时使用Boat
 */
public class Person {
    private String name;

    private Vehicles vehicles;

    public Person(String name, Vehicles vehicles) {
        this.name = name;
        this.vehicles = vehicles;
    }

    // 这里涉及到一个编程思路，就是可以把具体的要求，封装成方法
    public void passRiver(){ // 画图 -- 人找工厂 -- 工厂生产对应的工具 给人用
        // 过河 先得到船
        Boat boat = VehiclesFactory.getBoat();
        boat.work();
    }

    public void common(){ // 画图 -- 人找工厂 -- 工厂生产对应的工具 给人用
        // 过河 先得到马
        Hores hores = VehiclesFactory.getHorse();
        hores.work();
    }

}


class HomeWork04 {
    public static void main(String[] args) {
        Person tang = new Person("唐僧", new Hores()); // 默认的交通工具是马
        tang.common();
        tang.passRiver();
    }
}