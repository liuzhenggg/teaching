package HomeWork.work06.w04;

// 交通工具工厂类
public class VehiclesFactory {

    // 其内有获得交通工具的方法
    public static  Hores getHorse(){
        return new Hores();
    }

    // 因为是工厂类 所以我不想创建 工厂的对象 所以直接设置称为静态的
    public static Boat getBoat() {
        return new Boat();
    }

}
