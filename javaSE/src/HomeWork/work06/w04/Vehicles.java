package HomeWork.work06.w04;
// 将需求 转化为代码 是一个程序员 必备的能力

// 交通工具接口类
public interface Vehicles {

    // 交通工具的工作方法
    void work();

}
