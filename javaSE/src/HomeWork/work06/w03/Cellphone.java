package HomeWork.work06.w03;

// 这题是有点难 但是实际开发中 这样的写法很多 大家尽量掌握
public class Cellphone {
    // 有一个手机类Cellphone,定义方法testWork，测试计算功能，调用计算接口的work方法
    public void testWork(ICalculate iCalculate, double n1, double n2){
        double result = iCalculate.work(n1, n2);
        System.out.println("计算后的结果是=" + result);
    }

}

class Test{
    public static void main(String[] args) {
        Cellphone cellphone = new Cellphone();

        // 这里是匿名内部类的写法 传进去的是一个匿名对象 同时把参数传进去
        cellphone.testWork(new ICalculate() {
            // 匿名内部类直接重写 方法 设计计算规则
            @Override
            public double work(double n1, double n2) {
                return n1 - n2;
            }
        },5 , 8);

    }
}