package HomeWork.work06.w03;

public interface ICalculate {

    // work方法，是完成计算，但是题没有具体要求，所以自己设计
    // 至于该方法完成怎样的计算，我们交给匿名内部类完成
    double work(double n1,double n2);

}
