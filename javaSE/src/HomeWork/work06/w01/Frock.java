package HomeWork.work06.w01;
/**
 1.编程题： 一个类 写不下 【用软件包 】

 1.1在 Frock类中声明私有的静态属性 currentNum [int类型]，初始值为 10000，作为衣服出厂的序列号起始值。

 1.2 声明公有的静态方法 getNextNum，作为生成上衣唯一序列号的方法。每调用一次，将currentNum 增加100 并返回值

 1.3在TestFrock类的main方法中，分两次调用getNextNum方法，获取序列号并打印输出.

 1.4在Frock类中声明 serialNumber(序列号)属性，并提供对应的get方法；

 1.5在Frock类的构造器中，通过调用getNextNum方法为Frock对象获取唯一序列号，赋给serialNumber 属性。

 1.6在 TestFrock 类的 main 方法中，分别创建三个 Frock 对象，并打印三个对象的序列号，验证是否为按100递增。

 */
public class Frock {

    // 衣服出厂的序列号起始值
    private static int currentNum = 10000;

    private int serialNumber;

    public int getSerialNumber() {
        return serialNumber;
    }

    public Frock() {
        this.serialNumber = getNextNum();
    }

    // 生成上衣唯一序列号
    public static int getNextNum(){
        currentNum += 100;
        return currentNum;
    }

}
