package HomeWork.work06.w01;

public class TestFrock {
    public static void main(String[] args) {
        // 可以使用对象 调用 也 可以使用 类调用

        int number =  Frock.getNextNum();// 第一次调用

        int number2 =  Frock.getNextNum();// 第二次调用

        System.out.println(number + " " + number2); // 思考为什么 number 不等于 number2 【内存图】

        // 打印三个对象
        Frock f1 = new Frock();
        System.out.println(f1.getSerialNumber());
        // System.out.println(f1.getSerialNumber());  同一个对象不会 再累加了 注意内存图
        Frock f2 = new Frock();
        System.out.println(f2.getSerialNumber());
        Frock f3 = new Frock();
        System.out.println(f3.getSerialNumber());

    }
}
