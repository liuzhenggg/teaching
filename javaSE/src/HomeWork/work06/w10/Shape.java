package HomeWork.work06.w10;

// 形状的抽象类 里面有 定义 计算周长 和 面积的方法
public abstract class Shape {

    // 计算周长
    public abstract double CalPerimeter();

    // 计算面积
    public abstract double CalArea();

}

// 圆形类
class Round extends Shape{
    private static final double P = 3.1415926;

    // 半径
    double radius;

    @Override // 计算圆的周长  C=2πr
    public double CalPerimeter() {
        return P * radius * 2;
    }

    @Override // S=πr²
    public double CalArea() {
        return P * radius * radius;
    }

    // 创建对象时就要初始化 对应的半径
    public Round(double radius) {
        this.radius = radius;
    }
}

// 正方形类
class Square extends Shape{
    // 边长
    double sideLength;
    @Override // 周长
    public double CalPerimeter() {
        return sideLength * 4;
    }
    @Override  // S=a*a
    public double CalArea() {
        return sideLength * sideLength;
    }

    public Square(double sideLength) {
        this.sideLength = sideLength;
    }
}

class W10{
    public static void main(String[] args) {
        Round round = new Round(12.1);
        System.out.println(round.CalArea());
        System.out.println(round.CalPerimeter());
    }
}