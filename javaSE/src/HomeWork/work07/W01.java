package HomeWork.work07;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
1.编程题：
将字符串中指定部分进行反转。比如将 “abcdef”  反转为 “aedcbf”
reverse(String str, int start, int end) // start：开始翻转的字符索引 end：结束翻转的字符索引

 */
public class W01 {
    public static void main(String[] args) throws Exception{
        //System.out.println(reverse("abcdefg", 2, 5));
        w3("");
        w4();
    }

    /*
        思路分析：
            1.先把方法定义确定
            2.把String 转成 char[], 因为 char[] 的元素顺序是可以交换的
     */
    public static String reverse(String str,int start, int end) {

        // 参数校验
        if (str == null || start < 0 || end > str.length() || start < end ) {
            throw new RuntimeException("参数非法！");
        }

       char[] chars =  str.toCharArray();
        char temp = ' '; // 交换的辅助变量
        // 只循环需要循环的部分 -- 然后互换位置
        for (int i = start, j =end; i < j ; i++, j--) {
            temp = chars[i];
            chars[i] = chars[j];
            chars[j] = temp;
        }
        // 使用 chars 重新构建一个 String 返回即可
        return new String(chars);
    }

    /*
        2.编程题：
            输入用户名、密码、邮箱，如果信息录入正确，则提示注册成功，否则抛出异常
            要求：
            （1.）用户名长度 小于六位
            （2.）密码的长度为六位
            （3.）邮箱中包含@ 和 .  并且@ 在 . 的前面
     */
    public static void w2(String name,String pwd, String email) {
        // 过关
        // 第一关
        int userLength = name.length();
        if (userLength > 6) {
            throw new RuntimeException("用户名长度最多为六位");
        }

        // 第二关
        int pwdLength = pwd.length();
        if (pwdLength != 6){
            throw new RuntimeException("密码长度必须为六位！");
        }

        // 第三关
        int i = email.indexOf('@');
        int j = email.indexOf('.');
        if (!(i > 0 && j > i)) {
            throw new RuntimeException("邮箱中包含@ 和 .  并且@ 在 . 的前面 ");
        }

        System.out.println("恭喜你注册成功！！");

    }

    // 思路分析
    /*
        1. 需要先去除掉 标点符号  --- Need to remove punctuation first
        2. 再跟据 空格进行分隔
     */
    public static void w3(String word) {
        word = "Need to remove punctuation, first!.";

        // regex 是正则表达式
        // 也可以用单个的替换方法
        String s1 =  word.replaceAll( "[.;!;,]","");

        // 切割
        String[] s = word.split(" ");
        System.out.println(s.length); // 单词的个数

        System.out.println(s1);
    }


    // 使用时间戳的方法
    public static void w4() throws Exception{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Date star = sdf.parse("2021-05-26");// 开始时间
        Date end = new Date();// 结束时间
        String format = sdf.format(end);
        Date endDate = sdf.parse(format);

        // 获取开始时间戳
        Long startTime = star.getTime();
        Long endTime = endDate.getTime();

        // 结束时间减开始时间
        Long num = endTime - startTime; // 相差的毫秒值
        Long day = num / 24 / 60 / 60 / 1000; // 小时 分钟 秒钟 毫秒
        System.out.println("相隔时间为：" + day + "天");

    }

}
