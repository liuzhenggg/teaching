package week14;

import java.util.HashSet;

@SuppressWarnings({"all"})
public class HashSet01 {
    public static void main(String[] args) {
        HashSet set = new HashSet();

        System.out.println(set.add("孙权"));// T
        System.out.println(set.add("周瑜"));// T
        System.out.println(set.add("黄盖"));// T
        System.out.println(set.add("孙权"));// F  计算完哈希值后发现是重复的 就不添加了

        set.remove("孙权");
        System.out.println("set =" + set); // 两个

        // 清空
        set.clear();
        // 2.  set = new HashSet(); 方法二

        set.add(new Student("袁绍")); // 这里可以添加成功吗？
        set.add(new Student("袁绍"));
        System.out.println(set);

        // 经典面试题来了
        set.add(new String("曹操")); // 这里可以添加成功吗？
        set.add(new String("曹操"));

        // 先计算 哈希值 再去 添加 【不能添加 相同数据/元素 的真正含义】

        System.out.println(set);
    }
}

class Student {
    String name;

    public Student(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}
