package week14;

import java.util.ArrayList;

/*
    泛型引入
 */
@SuppressWarnings({"all"})
public class Generic01 {
    public static void main(String[] args) {
        // 使用传统的方法来解决
        ArrayList arrayList = new ArrayList();
        arrayList.add(new Dog("旺财", 10));
        arrayList.add(new Dog("发财", 1));
        arrayList.add(new Dog("小黄", 5));

        // 假如我们程序员不小心 添加进去了一只猫？ --- 类型转换异常
        //  arrayList.add(new Cat("招财猫", 3));

        // 遍历 --- 这里 是 Object 因为 ArrayList 不知道你会放啥进来
        for (Object o : arrayList) {
            // 向下转型 Object -> Dog
            Dog dog = (Dog) o;
            System.out.println(dog.getName() + "-" + dog.getAge());
        }
    }
}

/*
1.编写程序，在 ArrayList 中，添加 3 个 Dog 对象
2.Dog 对象含有 name 和 age, 并输出 name 和 age (要求使用 getXXX)
 */
class Dog {
    private String name;
    private int age;

    public Dog(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

class Cat {
    private String name;
    private int age;

    public Cat(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}