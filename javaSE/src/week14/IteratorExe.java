package week14;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 迭代器
 */
public class IteratorExe {
    public static void main(String[] args) {
        Collection col = new ArrayList();


        col.add(new Book("三国演义", "罗贯中", 15.45));
        col.add(new Book("红楼梦", "曹雪芹", 15.45));
        col.add(new Book("西游记", "吴承恩", 15.45));

        Iterator iterator = col.iterator();

        while (iterator.hasNext()) { // 判断是否有还有数据
             Object next = iterator.next();
            System.out.println(next);
        }

        // 快捷键 itit


    }
}


class Book{
    private String name;
    private String author;
    private double price;

    public Book(String name, String author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                '}';
    }
}
