package week14;

import java.util.Comparator;
import java.util.TreeSet;
// 一句话 --- 可以自定义 排序规则
@SuppressWarnings({"all"})
public class TreeSet01 {
    public static void main(String[] args) {

        // 解读
        // 1. 当我们使用无参构造器，创建 TreeSet 时，任然是无序的
        // 2. 我希望 按照 字符大小进行排序
        // 3. 使用 TreeSet 提供的一个构造器，可以传入一个比较器 （以匿名内部类的方式）


        TreeSet treeSet = new TreeSet(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                // 至于 compareTo 这个方法里面是怎么做的 同学们有兴趣 可以去看下
                // 我们时间紧，这个不重要 就不带着进去看了
                // 自定义排序规则
                return ((String) o1).compareTo((String) o2);
            }
        });
        treeSet.add("jack");
        treeSet.add("tom");
        treeSet.add("sp");
        treeSet.add("a");

        System.out.println(treeSet);
    }
}
