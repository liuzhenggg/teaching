package week14;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Set 集合
 */
 public class SetMethod {

    public static void main(String[] args) {
        // 解读：
        // 1.  以 set 接口的实现类 HashSet 来讲解 Set 接口的方法
        Set<String> set = new HashSet();
        set.add("john");
        set.add("lucy");
        set.add("john");// 重复 元素 --- 会被覆盖
        set.add("jack");

        set.add(null);
        set.add(null);// 再次添加 null

        // 打印 结论
        // 1.set 接口的实现类的对象,不能存放重复的元素（重复的元素将被覆盖）
        // 2.set 接口的实现类的对象 存放数据是无序的（即添加的顺序和取出的顺序不一致）
        // 3.取出的顺序虽然不是添加时的顺序，但是他是固定的
        System.out.println(" set ==> " + set);

        // 遍历方式 1  迭代器
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        // 遍历方式 2  增强 for 循环

        // 遍历方式 3  流式编程

    }

}
