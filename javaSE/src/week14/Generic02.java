package week14;

public class Generic02 {
    public static void main(String[] args) {
        // E  代表着一种 数据类型  可以任意指定
        Person<String> P = new Person<String>("1"); // 这里 类型可以任意指定

        Person<Integer> p1 = new Person<Integer>(1);
    }
}

/*
泛型的作用是：可以在类声明时
通过一个标识表示类中某个属性的类型，
或者是某个方法的返回值的类型，
或者是参数类型
 */
class Person<E> {
    // String s;  想让这个 s 的类型 在创建对象时指定 【属性的类型】
    E s; // E 表示 s的数据类型，该数据类型在定义 Person 对象时指定

    public Person(E s) {
        this.s = s;
    }

    public E f() { // 方法的返回类型使用 E
        return s;
    }
}