package week14;
// 最后写上 --- 电话号码谱
// 这行代码是用来抑制警告的
@SuppressWarnings({"all"})
public class HashSetStructure {
    public static void main(String[] args) {
        // 模拟一个 HashSet 的底层 （HashMap 的底层结构）

        //1. 创建一个数组，数组的类型是 Node[]
        //2. 有些老师/面试官  会直接把 Node[] 数组称为 表 [table]
        Nodes[] table = new Nodes[3];
        System.out.println("table =" + table); // 现在 table 里是空的

        //3. 创建节点
        Nodes liuB = new Nodes("刘备",null);

        // 蜀国
        table[0] = liuB; // 数组里面只存了一个，其它都是用引用串起来的
        Nodes guanY = new Nodes("关羽", null);
        liuB.next = guanY; // 将关羽 节点 挂载到 刘备后面

        System.out.println("table = " + table);

        // 创建独立元素
        Nodes zhangF = new Nodes("张飞", null);
        guanY.next = zhangF;// 将张飞 这个节点 挂载到 关羽后面
        System.out.println("table=" + table);

        // 可以如法炮制 魏国~~~~
    }
}

class Nodes { // 节点， 存储数据，可以指向下一个节点，从而形成1链表
    Object item; // 存放数据
    Nodes next; // 指向下一个节点

    public Nodes(Object item, Nodes next) {
        this.item = item;
        this.next = next;
    }
}
