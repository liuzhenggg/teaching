package week14;

import java.util.HashSet;
import java.util.Objects;

public class HashSetExe {
    public static void main(String[] args) {
        HashSet<Employee> hashSet = new HashSet<>();


        // 需要在此之前简单的介绍一下 哈希值

        hashSet.add(new Employee("刘政", 1));
        hashSet.add(new Employee("刘政", 1));
        hashSet.add(new Employee("立风", 2));
        hashSet.add(new Employee("李江", 3));

        System.out.println("hashSet = " + hashSet);
    }
}

class Employee {
    private String name;

    private int age;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    // 如果 name 和 age 值相同， 则返回相同的 hase 值


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return age == employee.age &&
                Objects.equals(name, employee.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}