package week14;

import exe.oop01.T;

import java.util.ArrayList;
import java.util.List;

/**
 * 集合的常用方法
 */
public class CollectionMethod {
    public static void main(String[] args) {
        /*

            1.add 添加单个指定元素
            2.remove 删除指定元素
            3.contains:查找元素是否存在
            4.size: 获取元素个数
            5.isEmpty:判断是否为空
            6.clear:清空
            7.addAll:添加多个元素
            8.containsAll:查找多个元素 是否都存在
            9.remoceAll: 删除多个元素

         */
        List list = new ArrayList<>();
        list.add("jack");
        list.add(10);
        list.add(true);

        System.out.println(list);
        list.remove("jack"); // 也可根据下标
        System.out.println(list);
        System.out.println(list.contains(10));
        // addAll 可以直接加集合
        List list2 = new ArrayList<>();
        list2.add("西游记");
        list2.add("红楼梦");
        list.addAll(list2);
        System.out.println(list);
        // 删除
        list.removeAll(list2);
        System.out.println(list);


    }
}
