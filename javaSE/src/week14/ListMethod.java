package week14;

import java.util.ArrayList;
import java.util.List;

public class ListMethod {
    public static void main(String[] args) {
        /*
        add(int index, Object ele） 在 index 位置插入 ele 元素
        addAll(int index, Collection eles) 从index 位置开始将 eles 集合中所有元素添加
        get (int index) 获取指定 index 位置的元素
        indexOf(Obj obj) 返回 obj 在集合中首次出现的位置
        int lastIndex (Obj obj) 返回obj 在当前集合中最后一次出现的位置
        remove(int index) 移除指定 index 位置的元素，并返回此元素
        set(int index,Obj ele) 设置指定位置元素为ele 相当于替换
        subList(int from, int toin) 返回从 from 到 toin 位置的子集合

         */
        List list = new ArrayList();
        list.add("亚瑟");
        list.add("后羿");

        list.add(1, "鲁班");
        System.out.println(list);

        List list1 = new ArrayList();
        list1.add("李白");
        list1.add("杜甫");
        list.addAll(1, list1);
        System.out.println(list);

        // 其实修改 也可以说是替换
    }
}
