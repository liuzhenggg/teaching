package week14;

import java.util.ArrayList;

/*
    ArrayList 的底层操作机制和源码分析
 */
public class ArrayListSource {
    public static void main(String[] args) {
        System.out.println();
        // 1. 使用 默认的无参构造器 来初始化 集合容器
        ArrayList list = new ArrayList();

        // 向其中添加 10 条数据
        for (int i = 1; i <= 10; i++) {
            list.add(i);
        }

        // 向其中添加 5 条数据
        for (int i = 11; i <= 15; i++) {
            list.add(i);
        }


        list.add(100);
        list.add(200);
        list.add(null);
    }
}
