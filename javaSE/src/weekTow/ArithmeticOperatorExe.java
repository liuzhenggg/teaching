package weekTow;

/**
 *  随堂练习 算数运算符
 */
public class ArithmeticOperatorExe {
    public static void main(String[] args) {
        // 需求：
        /*
            假设还有 59天放假，问：折合下来是几个星期零几天？
            思路分析：
                使用 int 变量 days 保存天数
                一个星期7天 可使用除法获取, 余数可用来表示天数
         */
        int days = 142;
        //  除法获取星期
        int weeks = days / 7;
        //  余数获取 天数
        int leftDays = days % 7;
        System.out.println(days + "有："+weeks+ "周"+"剩："+leftDays+"天");

    }
}
