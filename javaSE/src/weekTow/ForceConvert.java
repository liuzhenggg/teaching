package weekTow;

/**
 * 强制类型转换
 */
public class ForceConvert {
    public static void main(String[] args) {

        // 造成精度丢失
        int i = (int) 2.5;
        System.out.println(i);

        // 造成数据溢出
        int j = 200;
        byte b1 = (byte) j;
        // -64 + 8 = 56
        System.out.println(b1);
    }
}
