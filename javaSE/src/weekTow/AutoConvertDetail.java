package weekTow;

/**
 * 自动类型转换细节
 */
public class AutoConvertDetail {
    public static void main(String[] args) {

        int n1 = 10;
        float di = n1 + 1.1F;

        // (byte , short ）和char之间不会互相转换
        byte b1 = 10; // -128 -- 127
        int n2 = 1; // n2 是 int
        // byte b2 = n2;  // 报错， 原因：如果是变量赋值，判断类型 （因为类型已经分配了空间了）
        // char c1 = b1;   报错了，原因如上



    }
}
