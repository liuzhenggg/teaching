package weekTow;

/**
 * 关系运算符
 */
public class RelationalOperator {
    public static void main(String[] args) {

        // ps : 实际开发过程中不要使用这种意义不明的 标识符
        int a = 9;
        int b = 8;

        System.out.println(a>b);
        System.out.println(a>=b);
        System.out.println(a<b);
        System.out.println(a==b);
        System.out.println(a != b);
        boolean flag = a > b;
        System.out.println(flag);

    }

}
