package weekTow;

/**
 * 算数运算符详解
 */
public class ArithmeticOperator {

    public static void main(String[] args) {

        // 同学们猜一猜是几? 按照数学来说是 2.5
        // 但是两个都是整型的 算出来就为2
        System.out.println(10 / 4);
        // 这个时候最高精度就是 double 就有小数了
        // 2.5
        System.out.println(10.0 / 4);
        // 2.0
        double d = 10 / 4;
        System.out.println(d);
        // % 的本质是一个公式   a%b  = a - a / b * b
        System.out.println(10%3);

        // -10 - (-10) / 3 * 3 = -10 - (-9) = -1
        System.out.println(-10%3);
        System.out.println(10 % -3 );

        // ++ 的使用
        int i = 10;
        i++; // 自增 等价于 i = i + 1
        ++i; // 自增 等价于 i = i + 1
        System.out.println("i="+i); // 12

        /*
        作为表达式使用
        前++：++i先自增后赋值
        后++：先赋值后自增
         */
        int j = 6;
        // 等价于 j = j+1 ; k =j   所以两个都是 7
//        int k = ++j;

        // 等价 k = j; j=j+1  所以一个为6 一个为7
        int k = j++;
        System.out.println("k=" + k + "j=" + j);





    }


}
