package weekTow;

/**
 * 自动类型转换
 */
public class AutoConvert {
    public static void main(String[] args) {
        // 演示自动类型转换
        int num = 'a';  // char --> int
        int d = 80;
        double d1 = d; // int --> double
        System.out.println(num); // 97
        System.out.println(d1); // 80.0
    }

}
