package week10;

import java.io.FileInputStream;

public class Exception01 {
    public static void main(String[] args) {

        try{
            int num1 = 10;
            int num2 = 0;
            // 当抛出异常后，程序就崩溃了，下面的代码不执行
            // 大家想想这样的程序好吗？不好 不应该出现了一个不算致命的问题，就导致整个系统的崩溃
            // 所以 java 设计者，提供了一个叫 异常处理机制来解决该问题

            int res = num1 / num2;
        }catch (Exception e){
            System.out.println("异常信息= "+e.getMessage() );
        }

        System.out.println("程序继续执行~~~~~~~");
    }


    // 编译时异常 --- 必须马上处理的 案例
    public void test(){
        FileInputStream fis;

        try {
            fis = new FileInputStream("");
            int len;
            fis.close();
        }catch (Exception e) {
            System.out.println(e);
        }

    }




}
