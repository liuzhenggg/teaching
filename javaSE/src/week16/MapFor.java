package week16;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * map 集合的遍历方式
 */
public class MapFor {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();

        map.put("1","胡锦");
        map.put("2","张安婷");
        map.put("3","梁鑫睿");
        map.put("4","施静雅");
        map.put("5","王均钊");
        map.put("6","潘智鑫");
        map.put("7","李婉倩");
        map.put("8","乐天怡");
        map.put("9","阮俊杰");
        map.put("10","李创");

        // 第一组：先取出 所有的 key 通过 key 取出对应的 Value
        Set<String> key = map.keySet();
        for (String s : key) {
            System.out.println(s + "-" + map.get(s));
        }

        // 2. 使用迭代器
        Iterator<String> iterator = key.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next() + "-" + map.get(iterator.next()));
        }

        // 3.
        key.stream().forEach(x -> System.out.println(map.get(x)));
    }
}
