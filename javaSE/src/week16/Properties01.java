package week16;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

public class Properties01 {
    public static void main(String[] args) throws Exception{
        Properties ps = new Properties();
        //  ps.put("45", 4);

        ps.setProperty("name", "1232");
        ps.setProperty("pwd", "456");
        ps.store(new FileOutputStream("D:\\pro.txt"),"info");


        // 输入：
        ps.load(new FileInputStream("D:\\pro.txt"));
        String name = ps.getProperty("name");
        String pwd = ps.getProperty("pwd");
        System.out.println(name);
        System.out.println(pwd);
    }
}
