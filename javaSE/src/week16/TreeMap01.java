package week16;

import java.util.Comparator;
import java.util.TreeMap;

public class TreeMap01 {
    public static void main(String[] args) {
        TreeMap<String, String> treeMap = new TreeMap<>(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
               // return ((String) o2).compareTo((String) o1); 改完之后 从 Z --> A
                // 按照长度呢？
                return ((String) o1).length() - ((String) o2).length();
            }
        });

        // 默认是按照 首字母 从 A -- Z
        treeMap.put("jack", "杰克");
        treeMap.put("tom", "汤姆");
        treeMap.put("kristina", "克瑞斯缇娜");
        treeMap.put("smith", "史密斯");

        System.out.println(treeMap);
    }
}
