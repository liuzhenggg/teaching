package week13;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;

public class StringTest {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入数字：");
        // 数字太长了 不能使用 int 或 long 不然会越界
        String numberStr = sc.next();

        System.out.println(numberStr);
        boolean check = checkNumber(numberStr); // 参数校验
        if (!check){
            System.out.println("参数非法！！");
            return;
        }
        // 最终结果运算
        calStr(pingJie(numberStr));
    }

    public static void calStr(String[] strings){
        // 拿到数组后，计算值
        int index = strings.length / 2;
        int count = 0;
        int reg = 0;
        for (int i = 0; i < index; i++) {
           // 第一次特殊处理
            if (i == 0){
                Integer n1 = Integer.valueOf(strings[i]);
                Integer n2 = Integer.valueOf(strings[++reg]);
                count = n1 + n2;
                System.out.println(n1 + "+" + n2 + "=" + count);
                continue;
            }
            // 后面的正常打印
            Integer n1 = Integer.valueOf(strings[++reg]);
            Integer n2 = Integer.valueOf(strings[++reg]);
            count = n1 + n2;
            System.out.println(n1 + "+" + n2 + "=" + count);
        }
    }

    public static String[] pingJie(String numberS){
        // String numberS = "102345678910";
        int index = numberS.length() / 2;
        String[] strNumbers = new String[index];
        // 循环拆分字符串 拿到每组数字
        for (int i = 0; i < index ; i++) {
           // 拼成字符串
          String s1 =  String.valueOf(numberS.charAt(i));
          String s2 =  String.valueOf(numberS.charAt(numberS.length() - (i+1)));
          // 添加到 字符串数组里面 准备后续做加法运算
            strNumbers[i] = s1 + s2;
        }
        System.out.println(Arrays.toString(strNumbers));
        return strNumbers;
    }

    // 1. 输入格式校验
            // 1.0 校验是否都是 整数
            // 1.1 长度不能大于 100 位的正整数
            // 1.2 最高位 一定不是 0
            // 1.3 位数是 4 的倍数
    public static boolean checkNumber(String numberStr){
        // 但是这里要校验用户输入的是否是数字 -- 使用正则表达式
        if (!numberStr.matches("\\d+")){
            System.out.println(numberStr);
            System.out.println("请全部输入数字!!");
            return false;
        }
        if (numberStr.length() > 100){
            System.out.println("数字长度最多为100位");
            return false;
        }else if (numberStr.charAt(0) == '0'){
            System.out.println("最高位不能为0");
            return false;
        }else if (numberStr.length() %4 != 0 ){
            System.out.println("数字的位数必须是4的倍数");
            return false;
        }
        return true;
    }
}
