package week3;

/**
 * BreakDetail 使用细节
 */
public class BreakDetail {
    public static void main(String[] args) {
        // 外层循环
        label1:
        for (int j = 0; j < 4; j++) {

            // 内层循环
            label2:
            for (int i = 0; i < 10; i++) {
                if (i == 2) {
                    break label1;
                }
                System.out.println("i = " + i);
            }

        }
    }


}
