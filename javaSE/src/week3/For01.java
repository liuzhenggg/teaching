package week3;

/**
 * 循环结构
 *
 * 1. 循环变量初始化
 * 2. 循环条件
 * 3. 循环操作
 * 4. 循环迭代
 *
 */
public class For01 {
    public static void main(String[] args) {
        // 传统写法 -- 不可取
        for (int i = 1; i <= 10; i++) {
            System.out.println("同学们~~~~"+i);
        }
    }
}
