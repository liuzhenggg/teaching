package week3;

/**
 * Return01
 */
public class Return01 {
    public static void main(String[] args) {
        for (int i = 1; i <=5 ; i++) {
            if (i == 3) {
                System.out.println("你好 " + i);
                return;
            }
            System.out.println("hello word!");

        }
        // 这里就直接没有了
        // 如果是写在方法里会直接退出方法
        System.out.println("程序继续执行");

    }
}
