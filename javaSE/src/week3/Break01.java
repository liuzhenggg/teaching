package week3;

import java.util.Random;

/**
 * Break 练习
 */
public class Break01 {
    public static void main(String[] args) {

        break01();
    }


    public static void break01() {
        Random random = new Random();

        // 执行次数
        int i = 0;
        int number = 0;
        while (true){
            int randomNumber = random.nextInt(101);
            // 打印抽到的数字
            System.out.println(randomNumber);
            if (randomNumber == 90){
                number = randomNumber;
                break;
            }
            // 打印执行次数
            i++;
        }

        System.out.println("一共抽了 "+ i + "次" + "才抽到 "+ number);


    }
}
