package week3;

import java.util.Scanner;

/**
 * 多重循环练习题
 */
public class MulForExercise01 {

    /*
    外层循环控制行数（1到9），内层循环控制每一行中的列数，并且内层循环的结束条件是不大于外层循环当前的变量值。
    每个乘法表达式后面都跟着一个\t制表符用于对齐输出。
    每一行结束后，打印一个换行符\n以换行。
     */
    public static void main(String[] args) {
        exe01();
    }

    // 九九乘法表 -- 更简单的写法
    public static void exe01(){
        //  外层循环控制行数（1到9）
        for (int i = 1; i <= 9; i++) {
            // 内层循环控制每一行中的列数
            for (int j = 1; j <= i; j++) {
                System.out.print(j + "*" + i + "=" + i * j + "\t");
            }
            System.out.println();
        }
    }

    /*
        化繁为简
        先死后活

        1.先打印一个矩形
        *****
        *****
        *****

        2.打印半个金字塔
        *
        **
        ***
        ****
        *****

        3.打印完整金字塔 2 * 层数 -1

        4.加上每层之前的空格
     */

    // 打印金字塔 [半个]
    public static void exe02() {
        // 先打印一个矩形
        // 外层控制层数
        for (int i = 1; i <=5 ; i++) {
            // 内层控制个数
            for (int j = 1; j <= i; j++) {
                // 这里是每层个数
                System.out.print("*");
            }
            System.out.println("");
        }
    }

    // 整个 但是缺少空格
    public static void exe03(){
        for (int i = 1; i <=5 ; i++) {
            // 内层控制个数
            for (int j = 1; j <= 2 * i - 1; j++) {
                // 分析每层个数
                System.out.print("*");
            }
            System.out.println("");
        }
    }

    // 最后的完整金字塔代码
    public static void exe04(){
        for (int i = 1; i <=5 ; i++) {
            // 在输出 * 之前，还要输出对应的空格  = 总层数 - 当前层数
            for (int k =1; k <=5 -i;k++){
                System.out.print(" ");
            }
            // 内层控制个数
            for (int j = 1; j <= 2 * i - 1; j++) {
                // 分析每层个数
                System.out.print("*");
            }
            System.out.println("");
        }
    }

    // 先死后活--改层数
    public static void exe05(){
        System.out.println("请输入金字塔层数： ");
        Scanner scanner = new Scanner(System.in);
        int totalLevel = scanner.nextInt();
        System.out.println("金字塔层数为： " + totalLevel +" 层");

        // 控制层数
        for (int i = 1; i <=totalLevel ; i++) {

            // 在输出 * 之前，还要输出对应的空格  = 总层数 - 当前层数
            for (int k =1; k <=totalLevel -i;k++){
                System.out.print(" ");
            }

            // 内层控制**的个数
            for (int j = 1; j <= 2 * i - 1; j++) {
                // 分析每层个数
                System.out.print("*");
            }
            System.out.println("");


        }
    }

}
