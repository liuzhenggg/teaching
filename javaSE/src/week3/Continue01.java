package week3;

/**
 * Continue 代码案例
 */
public class Continue01 {
    public static void main(String[] args) {

    }

    // 猜猜是几
    public static void continue01() {
        int i = 1;
        while (i <= 4) {
            i++;
            if (i == 2) {
                continue;
            }
            System.out.println("i =" + i);
        }
    }


}
