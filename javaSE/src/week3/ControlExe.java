package week3;

import java.util.Scanner;

/**
 * 控制结构练习
 */
public class ControlExe {
    public static void main(String[] args) {
        controlExe04();
    }

    // 完成下面的表达式输出
    public static void controlExe01(){
        for (int i = 0; i <=5 ; i++) {
            System.out.println(i + "+" + (5-i) + " = 5");
        }
    }

    // 定义两个变量int,键盘输入整数，判断二者的和，是否能被3又能被5整除，打印提示信息
    public static void controlExe02(){
        int a, b;
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入变量a的值：");
        a = scanner.nextInt();
        System.out.println("请输入变量b的值：");
        b = scanner.nextInt();
        // 判断 二者的和 ---> 输入的 sum 值是不确定的，sum 有无数种
        int sum = a + b;
        System.out.println("a 和 b 的和 sum 的值为：" + sum);
        if (sum % 3 == 0 && sum % 5 == 0) {
            System.out.println("sum 能同时被3和五整除");
        }else if (sum % 3 == 0){
            System.out.println("sum 只能被3整除");
        } else if (sum % 5 == 0) {
            System.out.println("sum 只能被5整除");
        }else {
            System.out.println("sum 不能被3整除，也不能被5整除");
        }
        System.out.println("程序继续执行~~~~~~~~");
    }

    // 跟据输入的指定月份，打印该月份所属的季节。
    // 3，4，5 为春季
    // 6，7，8为夏季
    // 9，10，11为秋季
    // 12，1，2 为冬季
    public static void controlExe03(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入指定月份：");
        int month = scanner.nextInt();
        // 1. 穿透特性  2.应用场景
        switch (month) {
            case 3:
            case 4:
            case 5:
                System.out.println("春季");
                break;
            case 6:
            case 7:
            case 8:
                System.out.println("夏季");
                break;
            case 9:
            case 10:
            case 11:
                System.out.println("秋季");
                break;
            case 12:
            case 1:
            case 2:
                System.out.println("冬季");
                break;
            default:
                System.out.println("你输入的月份不合法");
        }
    }

    // 打印40—200 之间的所有偶数 （使用while）
    public static void controlExe04(){
        int i = 40;
        int count = 0; // 统计个数
        int sum = 0;  // 求和
        while (i <= 200) {
            // 循环体
            if (i%2 == 0){
                // 是偶数
                System.out.println("i 是偶数 i 为: "+ i);
                count++;
                sum += i;
            }
            // 循环变量迭代
            i++;
        }
        System.out.println("一共有 "+count+" 个偶数"+ "它们的和是 "+ sum);
    }
    // 参加歌手比赛，如果初赛成绩大于8.0进入决赛
    // 否则提示进入复活赛。并跟据性别提示进入男子组或女子组
    public static void controlExe05(){
        Scanner scanner = new Scanner(System.in);
        // 获取比赛成绩
        double score =  scanner.nextDouble();
        // 判断比赛成绩
        if (score > 8.0) {
            System.out.println("恭喜你进入决赛~~~");
            // 判断 选手性别
            System.out.println("请输入你的性别：");
            char gender = scanner.next().charAt(0);
            if (gender == '男'){
                System.out.println("请进入男子组~~");
            }else {
                System.out.println("请进入女子组~~");
            }
        }else {
            System.out.println("很遗憾，你为进入决赛，请准备复活赛~~~");

        }

    }

}
