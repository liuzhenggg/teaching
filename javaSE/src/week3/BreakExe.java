package week3;

import java.util.Scanner;

/**
 * Break 练习
 */
public class BreakExe {
    public static void main(String[] args) {
        breakExe02();
    }


    // 1—100 以内的数求和，当和第一次大于20时跳出循环
    public static void breakExe01() {
        int sum = 0;
        for (int i = 1; i <= 100; i++) {
            sum += i; // 累加
            if (sum > 20){
                System.out.println("和>20时候 当前数i=" + i);
                break;
            }

        }
    }

    public static void breakExe02() {
        Scanner scanner = new Scanner(System.in);
        String name = "";
        String passwd = "";
        int chance = 3; // 登陆一次就减少一次
        for (int i = 1; i <= 3; i++) {
            System.out.println("请输入名字");
            name = scanner.next();
            System.out.println("请输入密码");
            passwd = scanner.next();
            if ("张三".equals(name) && "666".equals(passwd)){
                System.out.println("欢迎"+name+"用户，登录成功");
                break;
            }
            // 登录机会减少一次
            chance--;
            System.out.println("你还有"+chance+"登录机会");

        }

    }
}
