package week3;

import java.util.Scanner;

/**
 * 嵌套分支练习 --- 给同学们 5分钟时间练习
 */
public class Exe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // 输入月份 和年龄
        System.out.println("请输入月份：");
        int month = scanner.nextInt();
        System.out.println("请输入年龄：");
        int age = scanner.nextInt();
        // 现实工作中，这里一定要加上参数校验。参数校验 ---- 略
        // 因为你不知道你的用户会给你输入一些什么乱七八糟的数据
        System.out.println("你输入的月份是："+month + "你输入的年龄是： " + age);
        if (month >= 4 && month <= 10 ) {
            // 这里是旺季
                //判断年龄
            if (age<=18) {
                // 小盆友
                System.out.println("旺季，收小朋友半价30块");
            }else if (age<=60) {
                // 大朋友
                System.out.println("旺季，收大朋友全价60块");
            }else {
                // 老朋友
                System.out.println("旺季，收老朋友全价20块");
            }
        }else {
            // 这里是淡季
                // 判断年龄
            if (age >= 18 && age<= 60){
                System.out.println("成年人，收你40块");
            }
            System.out.println("其他人群，收你 20块");
        }

    }
}
