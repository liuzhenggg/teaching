package week3;

/**
 * for 循环细节
 * 记住顺序，，循环变量最后操作
 */
public class ForDetail {
    public static void main(String[] args) {
        test();

    }

    // 会执行 几次？ 3 次
    //
    public static void test() {
        int count = 3;
        for (int i = 0, j = 0; i < count; i++, j += 2) {
            System.out.println("i=" + i + "j=" + j);
        }
    }

    public static void test01(){
        // 作用域
        int i = 1; // 循环变量初始化
        // 循环位置条件不能变化

        for (; i <=10 ;) {
            System.out.println("11111");
            i++; // 这个不能放外面 --- 不然就会无限循环下去，---看执行顺序
        }

        System.out.println(i); // 让同学们猜一猜这个 i 的值   11

        int j = 1;


        // 死循环
        for (;;) {
            System.out.println("你好"+j++);
        }
    }

}
