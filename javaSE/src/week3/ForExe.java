package week3;

/**
 * for 循环练习
 *
 * 编程思想：
 * 化繁为简 --- 一步一步分析需求
 * 先死后活 --- 改条件 1~~~100
 */
public class ForExe {
    public static void main(String[] args) {
        exe01();
    }

    /*
        思路分析
        化繁为简：
            1.完成输出 1--100 的值
            2.在输出的过程中进行过滤  %
            3.统计个数 定义一个变量 int count = 0 ; 当条件满足时 count ++

            4.定义一个变量 int sum = 0; 当条件满足时累计 sum += i;
         先死后活：
            把范围开始值 和结束值 都做成变量
            把取模 9 做成变量
     */

    public static void exe01() {
        int count = 0; // 统计个数的变量
        int sum = 0; // 统计总和

//        int start = 1
//        int end = 100
        for (int i = 1; i < 100; i++) {
            if (i % 9 == 0) {
                System.out.println("i =" + i);
                count++;
                sum += i;
            }
        }

        System.out.println("count "+ count + "个");
        System.out.println("sum "+ sum +"总和");
    }

}
