package week3;

import java.util.Scanner;

/**
 * if 分支控制语句演示
 */
public class If01 {

    public static void main(String[] args) {
    if03();

    }

    // 单分支
    public static void if01() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("小伙儿这次Java考了多少分呢？");
        int i = scanner.nextInt();
        if (i>=90) {
            System.out.println("小伙儿，你Java学的真六！");
        }

        System.out.println("程序继续~~~~~");
    }


    // 双分支结构
    public static void if02() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("小伙儿这次Java考了多少分呢？");
        int i = scanner.nextInt();
        if (i>60) {
            System.out.println("及格了，继续加油！");
        }else {
            System.out.println("小伙儿，你没及格了，被逮到了，老实补考去吧!");
        }

    }


    // 多分支结构
    public static void if03() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("小伙儿这次Java考了多少分呢？");
        int i = scanner.nextInt();
        // 参数校验
        if (i < 0 || i > 100){
            System.out.println("请输入合法成绩!");
            return;
        }
        if (i >= 90) {
            System.out.println("666666~~~~~");
        } else if (i >= 80) {
            System.out.println("也很不错哟");
        } else if (i >= 70) {
            System.out.println("继续加油！");
        } else if (i >= 60) {
            System.out.println("有点危险!差点补考");
        } else {
            // 可以去掉 else 我就不补考。。。。
            System.out.println("小伙儿，要准备补考了呀!");
        }


    }

}
