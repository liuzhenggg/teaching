package week3;

/**
 * DoWhile 循环，案例演示
 */
public class DoWhile01 {
    public static void main(String[] args) {
        doWhile();
    }

    public static void doWhile(){
        // 循环变量初始化
        int i = 1;
        do {
            // 循环执行语句
            System.out.println("今天天气真不错 " + i);
            // 循环变量迭代
            i++;
            // 循环条件判断
        } while (i<=10);

        // 退出的时候，这个i 是等于 11 而不是 10
        System.out.println("退出 do..while,程序继续执行" + i);
     }
}
