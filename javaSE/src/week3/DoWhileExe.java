package week3;

import java.util.Scanner;

/**
 * do..while 随堂练习
 */
public class DoWhileExe {
    public static void main(String[] args) {
        doWhileExe04();
    }

    // 1. 打印 1 到 100
    public static void doWhileExe01(){
        int i = 1;
        do {
            System.out.println("i的值为：" + i);
            i++;
        } while (i <= 100);

        System.out.println("程序继续~~~~");
    }

    // 第二题 计算 1--100 的和
    public static void doWhileExe02(){
        int i = 1;
        int sum = 0;
        do {
            sum +=  i;
            i++;
        } while (i <= 100);

        System.out.println(sum);
    }

    // 统计1—200 之间能被5整除但不能被3整除的个数
    public static void doWhileExe03(){
        // 化繁为简
            // 1.do - while 输出 1-200
            // 2.过滤 能被5整除但不能被3整除的数 %
            // 3.统计满足条件的个数 int cont =0
        int i = 1;
        // 统计满足条件的个数
        int count = 0;
        do {
            if (i % 5 == 0 && i % 3 != 0) {
                System.out.println("i=" + i);
                count++;
            }
            i++;
        } while (i <= 200);

        System.out.println("count = " + count);
    }

    // 如果 法外狂徒 张三不还钱，则马老师将一直使用闪电五连鞭，直到张三说还钱为止
    public static void doWhileExe04(){
        // 1. 不停的问还钱吗？
        //2. 使用 char answer 接收回答
        Scanner scanner = new Scanner(System.in);
        char answer ;
        // 选择
        boolean ches = true;
        do {
            System.out.println("马老师问张三：还钱吗？ yes OR no");
            answer = scanner.next().charAt(0);
            // 默认进行了参数校验
            System.out.println("张三的回答是：" + answer);

            // 张三肯还钱了！
            if (answer == 'y') {
                System.out.println("张三肯还钱了，马老师放过了他。");
                ches = false;
            }else {
                System.out.println("张三不肯还钱!,马老师使出了闪电五连鞭，效果拔群，张三受到了10点伤害!");
                // 这里还能改呢？ 给张三加上生命值
            }
        } while (ches);
        System.out.println("程序继续执行~~~~~");

    }

}
