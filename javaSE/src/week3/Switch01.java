package week3;

import java.util.Scanner;
/**
 * Switch0 分支结构
 */
public class Switch01 {
    public static void main(String[] args) {
        switch01();
    }

    public static void switch01() {
        // 从键盘获取一个字符
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个字符（a-g）");
        // 获取我输入的第一个字符
        // char c1 = scanner.next().charAt(0);
        // System.out.println("我输入的是否是一个字节");
        boolean b = scanner.hasNextByte();
        System.out.println(b);
        // 97 也可以执行！！！！
        int i = scanner.nextInt();
        switch (i) {
            case 'a':
                System.out.println("今天星期一，同学们也要元气满满~~~~");
                break;
            case 'b':
                System.out.println("今天星期二，火力全开状态拉满~~~~~");
                break;
            case 'c':
                System.out.println("今天星期三，一周已过半~~~~");
                break;
            case 'd':
                System.out.println("今天星期四,期待放假日~~~~");
                break;
            case 'e':
                System.out.println("今天星期五，晚上打老虎~~~~");
                break;
            case 'f':
                System.out.println("今天星期六，放假睡大觉~~~~~");
                break;
            case 'g':
                System.out.println("今天星期天，我有点心不在焉~~~~");
                break;
            default:
                System.out.println("你输入的字符没有匹配");
                // default 后面有没有 break 已经不重要了
        }

        System.out.println("退出Switch，程序继续执行");

    }

}
