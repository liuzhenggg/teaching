package exe.oop01;

public class A01 {

    public double max(double[] arr) {

        if (arr == null){
            System.out.println("参数非法！！！");
            return 0.0;
        }
        // 筛选出最大的那个元素
        double number = arr[0];

        for (int i = 0; i < arr.length; i++) {
            if (number <= arr[i]){
                number = arr[i];
            }
        }

        return number;

    }
}
