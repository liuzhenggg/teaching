package exe.oop01;

/**
 * 面向对象---第一部分课后练习题
 */
public class T {

    /*

    1.编写类A01，定义方法max, 实现求某个 double 数组的最大值，并返回

    2.编写类A02，定义方法 find , 实现查找某字符串数组中的元素查找，并返回索引

    3.编写类 Book 定义方法 upDatePrice , 实现更改某本书的价格，具体：如果价格 > 150 ,则更改为 150，如果价格 > 100 更改为 100 否则不变

    4.编写类A03，实现数组的复制功能 copyArr, 输入旧数组，返回一个新数组，元素和旧数组一致

    5. 设计一个Dog类，有名字、颜色和年龄属性，定义输出方法 show() 显示其信息。并创建对象，进行测试

    6.代码分析题，分析编译后的输出结果。	exe【包名】

    7.定义Music类，里面有音乐名 name 、音乐时长times属性，并有播放 play 功能和返回本身属性信息的功能方法 getInfo

     */

    public static void main(String[] args) {
        A01 a01 = new A01();
        double[] d = {0.1, 11.3, 57.9, 999};
        double max = a01.max(d);
        System.out.println(max);

        A02 a02 = new A02();
        String[] s = {"1", "123", "3"};
        a02.find("123", s);

//        Book book = new Book();
//        book.price = 170;
//        book.upDatePrice(book);
//        System.out.println(book.price);

    }
}
