package exe.oop01;

public class A02 {

    // 2.编写类A02，定义方法 find , 实现查找某字符串数组中的元素查找，并返回索引
    public int find(String str,String[] strings){
        if (strings == null || str == null) {
            System.out.println("参数非法 ！！！");
            return 0;
        }

        int k = -1;
        for (int i = 0; i < strings.length; i++) {
            if (str.equals(strings[i])){
                System.out.println("找到了对应的元素：" + strings[i] + " 下标是：" + i);
                k = i;
            }
        }

        if (k == -1){
            System.out.println("没有找到对应的元素");
        }

        return k;

    }
}
