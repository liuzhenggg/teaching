package week4;

import java.util.Scanner;

/**
 * 数组快速入门案例
 */
public class Array02 {

    public static void main(String[] args) {
        array02();
    }

    // 1.创建一个 double 数组，大小 5
    // 2.循环输入
    public static void array02(){
        double[] scores = new double[5];

        Scanner scanner = new Scanner(System.in);
        // 向 收纳箱内放东西
        for (int i =0; i< scores.length ; i++){
            System.out.println("请输入第"+ (i+1) + "个元素的值");
            scores[i] = scanner.nextDouble();
        }

        // 输出，遍历  从收纳箱内取东西
        for (int i =0; i< scores.length;i++){
            System.out.println("第"+(i+1) + "个元素的值=" + scores[i]);

        }

    }



}
