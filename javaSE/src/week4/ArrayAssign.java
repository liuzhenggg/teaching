package week4;

/**
 * 数组赋值机制
 */
public class ArrayAssign {
    public static void main(String[] args) {
        arrayAssign();
    }

    public static void arrayAssign() {
        // 基本数据类型赋值
        int n1 = 10;
        int n2 = n1; // 值拷贝

        // n2 的变化是不会影响到n1
        n2 = 80;
        System.out.println("n1=" + n1); // 10
        System.out.println("n2=" + n2); // 80

        // 数组在默认情况下是引用传递，赋的值是地址，赋值方式为引用赋值
        // 是一个地址，arr2 变化会影响到 arr1
        int[ ]  arr1 = {1,2,3};
        int[ ]  arr2 = arr1;  // 把 arr1赋给 arr2
        arr2[0] = 10;

        //看看arr1 的值
        System.out.println(arr1[0]);


    }
}
