package week4;
/*
    数组的使用细节 详解
 */
public class ArrayDetail {
    public static void main(String[] args) {
        arrayDetail01();
    }
    /*
    1.数组是多个相同类型数据的集合，实现对这些数据的统一管理
    2.数组中的元素可以是任何类型的数据，包括基本类型和引用类型，但不能混用
    3.数组创建后，如果没有赋值，会给默认值
        整型: 0    浮点型: 0.0   字符型: \u0000  布尔型: false   字符串: null
    4.使用数组的三个步骤：
        4.1声明数组并开辟空间
        4.2给数组各个元素赋值
        4.3使用数组
    5.数组的下标是从0开始的
    6.数组下标必须在指定的范围内使用，否则会出现下标越界异常
    7.数组本身是属于引用类型
     */
    public static void arrayDetail01() {
        // 容器--桶子--装水--装油
        int[] array = new int[]{1,2,3};

        String[] strings = new String[] {"123"};

        // 3.数组创建后，如果没有赋值，会给默认值
        short[] shorts = new short[5];
        for (int i = 0; i < shorts.length; i++) {
            System.out.println(shorts[i]);
        }

        // 下标越界
        System.out.println(shorts[5]);

    }

}
