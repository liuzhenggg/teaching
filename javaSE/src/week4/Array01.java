package week4;

public class Array01 {

    public static void main(String[] args) {
        array02();
    }

    /*
        思路分析
        1.定义六个变量 double 求和 得到总体重
        2.平均体重 = 总体重 / 6
            3. 问题： 6 -》600 -》 566 -》 容器
        */
    public static void array01(){
        int[] array = new int[5];

    }


    // 用数组装
        // 1.我们可以通过 che[下标] 来访问数组元素
        //      下标是从 0 开始编号的比如第一个元素就是 hen[0] 第二个就是 hen[1]
    public static void array02(){

        double[] che = {3, 5, 1, 3.4, 2, 30};
        double total = 0;
        for (int i = 0; i < 6; i++) {
            System.out.println("第"+(i+1)+"个元素的值=" +che[i]);
            // 获得总体重
            total+=che[i];
        }

        System.out.println("总体重为：" + total + "平均体重为：" + total / 6);

    }

}
