package week4;

import java.util.Arrays;

/**
 * 冒泡排序
 */
public class BubbleSort {

    /*
       思路分析：
       分四轮去做

     */
    public static void main(String[] args) {
        bubbleSort02();
    }

    // 先死后活
    public static void bubbleSort02(){


        int[] array = {6, 9, 7, 4, 3,-1,-20,200};

        int temp = 0;

        for (int i = 0; i < array.length - 1; i++) {
            // array.length-1 - i 是怎么来的 ？
            for (int j = 0; j < array.length-1 - i; j++) {
                // 如果前面的数 > 后面的数，就交换
                if (array[j] > array[j + 1]){
                    // 交换  ---> 需要一个中间变量
                    // 先把 i 给temp
                    // 再把 j+1 给 j
                    // 再把 temp 给到 j+1
                    temp = array[j];
                    array[j] = array[j+1];
                    array[j + 1] = temp;
                }
            }

        }
        System.out.println(Arrays.toString(array));


    }

    public static void bubbleSort01(){
        int[] array = {6, 9, 7, 4, 3};

        int temp = 0;
        for (int j = 0; j < 4; j++) {
            // 如果前面的数 > 后面的数，就交换
            if (array[j] > array[j + 1]){
                // 交换  ---> 需要一个中间变量
                // 先把 i 给temp
                // 再把 j+1 给 j
                // 再把 temp 给到 j+1
                temp = array[j];
                array[j] = array[j+1];
                array[j + 1] = temp;
            }
        }

        System.out.println("第一轮排序后 array 数组的顺序是：" + Arrays.toString(array));

        System.out.println("=====下面进行第二轮排序=====");
        for (int j = 0; j < 3; j++) {
            // 如果前面的数 > 后面的数，就交换
            if (array[j] > array[j + 1]){
                // 交换  ---> 需要一个中间变量 --- 这里给同学们画图解析
                // 先把 i 给temp
                // 再把 j+1 给 j
                // 再把 temp 给到 j+1
                temp = array[j];
                array[j] = array[j+1];
                array[j + 1] = temp;
            }
        }
        System.out.println("第二轮排序后 array 数组的顺序是：" + Arrays.toString(array));

        System.out.println("=====下面进行第三轮排序=====");
        for (int j = 0; j < 2; j++) {
            // 如果前面的数 > 后面的数，就交换
            if (array[j] > array[j + 1]){
                // 交换  ---> 需要一个中间变量 --- 这里给同学们画图解析
                // 先把 i 给temp
                // 再把 j+1 给 j
                // 再把 temp 给到 j+1
                temp = array[j];
                array[j] = array[j+1];
                array[j + 1] = temp;
            }
        }
        System.out.println("第三轮排序后 array 数组的顺序是：" + Arrays.toString(array));

        System.out.println("=====下面进行第四轮排序=====");
        for (int j = 0; j < 1; j++) { // 比较一次
            // 如果前面的数 > 后面的数，就交换
            if (array[j] > array[j + 1]){
                // 交换  ---> 需要一个中间变量 --- 这里给同学们画图解析
                // 先把 i 给temp
                // 再把 j+1 给 j
                // 再把 temp 给到 j+1
                temp = array[j];
                array[j] = array[j+1];
                array[j + 1] = temp;
            }
        }
        System.out.println("第四轮排序后 array 数组的顺序是：" + Arrays.toString(array));

    }




}
