package week4;

/*
    数组拷贝
 */
public class ArrayCopy {
    public static void main(String[] args) {

    }

    // 数组拷贝
    public static void arrayCopy() {
        int[ ] arr1 = {10,20,30};

        // 需要一个新的数组
        int[] arr2 = new int[arr1.length];

        for (int i = 0; i < arr1.length; i++) {
            arr2[i] = arr1[i];
        }

        // 打印 arr2 的值
        for (int i = 0; i < arr2.length; i++) {
            System.out.println(arr2[i]);
        }

    }




}
