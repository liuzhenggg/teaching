package week4;

/**
 * 二维数组 快速入门案例
 */
public class TwoDimensionalArray01 {

    public static void main(String[] args) {
        twoDimensionalArray01();
    }


    public static void twoDimensionalArray01() {
        // 数组里面的每一个元素, 都是数组
        int[][] arr = {
                {1, 0, 0, 0, 5, 0},
                {0, 2, 0, 4, 0, 6},
                {0, 0, 3, 0, 0, 0}
        };

        System.out.println("二维数组的元素个数： " + arr.length);
        // 现在怎么打印呢？ --- 直接打印的是地址值

        // 外层循环 -- 数组的个数
        for (int i = 0; i < arr.length ; i++) {
            // 内层，循环打印每个数组
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println();
        }

        System.out.println("程序继续执行~~~~");

    }

}
