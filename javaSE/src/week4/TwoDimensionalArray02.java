package week4;

import java.util.Arrays;

/**
 * 二维数组初始化
 */
public class TwoDimensionalArray02 {

    public static void main(String[] args) {

        twoDimensionalArray03();
    }

    public static void twoDimensionalArray01(){
        int[][] arr = new int[2][3];

        // 它的每一个元素 仍然是一维数组，而不是具体的数字
        // 既然是一维数组，就要遵循一维数组引用存放规则
        arr[1][1] = 8;

        for (int i = 0 ; i< arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }

    public static void twoDimensionalArray02(){
        int[][] arr = new int[3][];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = new int[i + 1];
            // 赋值
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = j + 1;
            }

        }
        // 循环遍历
        for (int i = 0; i < arr.length; i++) {

            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static void twoDimensionalArray03(){
        int arr [ ][ ] ={{4,6},{1,4,5,7},{-2}};
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {

            for (int j = 0; j < arr[i].length; j++) {
                sum = sum + arr[i][j];
            }

        }
        System.out.println(sum);
    }

}
