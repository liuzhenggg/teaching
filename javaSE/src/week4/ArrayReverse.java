package week4;

import java.util.Arrays;

/*
    数组反转
 */
public class ArrayReverse {

    public static void main(String[] args) {
        arrayReverse01();
    }

    // 数组反转
    public static void arrayReverse01() {

        int[] array1 = {10, 20, 30, 40, 50, 60};

        System.out.println("数组 array1 的值为: "+ Arrays.toString(array1));

        int[] array2 = new int[array1.length];

        // 逆序遍历，从 5 向 0 遍历
        // 如何顺序遍历呢？
        for (int i = array1.length-1, j = 0; i >= 0; i--, j++) {
            array2[j] = array1[i];
        }

        // 让 array1 指向 array2 数据空间，此时 array1 原来的数据空间就没有变量应用了，就会被JVM 的GC机制回收掉
        array1 = array2;
        System.out.println("数组 array1 的值为: "+ Arrays.toString(array1));

    }


}
