package week4;
/*
    数组练习题
 */
public class ArrayExercise {
    public static void main(String[] args) {
        arrayExercise();
    }

    public static void arrayExercise() {

        int[] array = new int[32];
        // 循环赋值
        for (int i = 0; i < array.length; i++){
            array[i] = 1001 + i;
        }

        // 遍历数组
        for (int i = 0; i < array.length; i++) {
            System.out.println("第"+(i+1)+"位同学的学号是"+array[i]);

        }

    }


}
