package week12;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Date01 {
    public static void main(String[] args) throws ParseException {
//        Date d1 = new Date(); // 获取当前系统时间
//        Date d2 = new Date(5751247);// 通过指定毫秒获得时间
//        System.out.println(d1.getTime()); // 获取某个时间对应的毫秒数
//
//        // 1.创建 SimpleDateFormat 对象 可以指定相应的格式
//        // 2.这里的格式字母是规定好的，不能乱写
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
//        String formt = sdf.format(d1); // 将 d1 的时间格式化掉
//        System.out.println(formt);
//
//
//        // 1. 可以把一个格式化的String 转成对应的 Date
//        // 2. 得到 Date 仍然在输出时 还是 国外的形式，如果希望指定格式输出，需要转换
//        // 3. 在把 String 转 Date, 使用 sdf 格式需要和你给的 String 格式一样。
//        String s = "2023年01月01日";
//        Date parse = sdf.parse(s);
//        System.out.println(parse);
//        System.out.println(sdf.format(parse));

        Calendar c = Calendar.getInstance();
        c.set(2018,3,2);
        System.out.println(c.get(Calendar.YEAR));;
        System.out.println(c.get(Calendar.MONTH));;
        System.out.println(c.get(Calendar.DAY_OF_MONTH));;
        int cd =  c.get(Calendar.DAY_OF_WEEK);
        System.out.println(cd);
    }
}
