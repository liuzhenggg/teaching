package week12;

public class StringBuffer01 {
    public static void main(String[] args) {

        // 1.StringBuffer 的直接父类是 AbstractStringBuilder
        // 2.在父类中有属性 char[] value; 不是 final
        // 该 value 数组 存放的字符串内容就不在常量池了，而在堆里面【字符串缓冲区】
        // 4. StringBuffer 是一个 final类，不能被继承
        StringBuffer stringBuffer = new StringBuffer("hello");

    }
}
