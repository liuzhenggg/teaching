package week12;

/**
 *  String 类详解
 */
public class String01 {
    public static void main(String[] args) {

        //1.String 对象用于保存字符串，也虎眼石一组字符序列
        //2. "jack" 字符串常量，双引号括起来的字符序列
        //3. 字符串的字符使用 Unicode 字符编码, 一个字符占两个字节
        //4. Sting 有很多构造器，构造器的重载
        //5. Sting 类实现了接口 Serializable 【序列化/串行化 可在网络传输】
        //                    Comparable 【对象可以比较大小】
        //6. Sting 是 final 类 不可被继承
        //7. String 有属性  private final char value[]; 用于存放字符串内容
        //8. 一定要注意: value 是一个 final 类型 不可修改(一定注意 -- 指的是地址值不可修改 ---所以长度就不可修改了)
                // 即 value 不能指向新的地址，但是单个字符内容可以变化

        String name = "jack";
        name = "tom"; // name 是个变量 "jack" 才是常量  --- 这里创建了几个对象？ 创建了两个

        String s1 = "同学们真靓"; // 指向常量池
        String s2 = "你好java";
        String s3 = "你好java";
        String s4 = new String("你好java"); // 指向堆


        System.out.println(s2 == s3);
        System.out.println(s2 == s4);
        System.out.println(s2.equals(s3));
        System.out.println(s1 == s2);



    }

}
