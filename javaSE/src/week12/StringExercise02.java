package week12;

public class StringExercise02 {

    public static void main(String[] args) {
        Person p1 = new Person();
        p1.name = "你好";
        Person p2 = new Person();
        p2.name = "你好";

        System.out.println(p1.name.equals(p2.name)); // equals 源代码 比较的是内容
        System.out.println(p1.name == p2.name); // T
        System.out.println(p1.name == "你好");// T

        String s1 = new String("ABCD");
        String s2 = new String("ABCD");

        System.out.println(s1 == s2);


    }


}

class Person {
    String name;
}