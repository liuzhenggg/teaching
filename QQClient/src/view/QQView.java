package view;

import service.FileClientService;
import service.MessageClientService;
import service.UserClientService;

import java.util.Scanner;

public class QQView {
    public boolean loop = true; // 控制是否显示菜单
    public String key = ""; // 接收用户的键盘输入

    // 用户处理服务
    UserClientService userClientService = new UserClientService();

    // 消息处理服务
    MessageClientService messageClientService = new MessageClientService();

    // 文件处理服务
    FileClientService fileClientService = new FileClientService();

    // 显示主菜单
    private void mainMenu() {
        while (loop) {
            System.out.println("==============欢迎登录网络通讯系统==============");
            System.out.println("\t\t 1. 登录系统");
            System.out.println("\t\t 9. 退出系统");

            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入菜单编号：");
            key = scanner.next();

            // 跟据用户的输入来处理不同的业务逻辑
            switch (key) {
                case "1":
//                    System.out.println("登录系统的业务逻辑");
                    System.out.println("请输入用户编号: ");
                    String userId = scanner.next();
                    System.out.println("请输入密  码:  ");
                    String pwd = scanner.next();
                    // 判断当前用户是否合法
                    if (userClientService.checkUser(userId,pwd)){
                        System.out.println("===========欢迎 (用户 " + userId + ") ================");
                        //进入二级菜单
                        while (loop){
                            System.out.println("\n==========网络通信系统二级菜单(用户 " + userId + ") ================");
                            System.out.println("\t\t  1.显示在线用户列表");
                            System.out.println("\t\t  2.群发消息" );
                            System.out.println("\t\t  3.私聊消息" );
                            System.out.println("\t\t  4.发送文件" );
                            System.out.println("\t\t  9.退出系统" );
                            System.out.println("请输入你的选择: ");
                            key = scanner.next();
                            switch (key) {
                                case "1":
                                    // System.out.println("显示在线用户列表"); // 先把架子搭起来 后面换业务代码
                                    userClientService.onLineFriendList();
                                    break;
                                case "2":
                                    System.out.println("请输入你想对大家说的话: ");
                                    String msg = scanner.next();
                                    userClientService.sendMessageToAll(msg,userId);
                                    break;
                                case "3":
                                    System.out.println("请输入想聊天的用户名(在线): " );
                                    String getterID = scanner.next();
                                    System.out.println("请输入想说的话：");
                                    String content = scanner.next();

                                    // 编写一个方法 将这个消息发送给服务端
                                    messageClientService.sendMessageToOne(content,userId,getterID);

                                    break;
                                case "4":
                                    System.out.println("请输入文件接收者:");
                                    String getterId = scanner.next();
                                    System.out.println("请输入要传输的文件url:");
                                    String url = scanner.next();
                                    System.out.println("请输入你要把文件存放到对方电脑的什么位置: ");
                                    String dest = scanner.next();
                                    fileClientService.sendFileTOne(url,dest,getterId,userId);
                                    System.out.println("发送文件");
                                    break;
                                case "9":
                                    // 调用方法，给服务器发送一个退出系统的 message
                                    userClientService.logout();
                                    loop = false; // 退出循环

                                    break;
                            }
                        }
                    }else {
                        System.out.println("============登录服务器失败=================");
                    }

                    break;
                case "9":
                    System.out.println("退出系统");
                    loop = false;
                    break;
            }

        }
    }

    // 测试系统
    public static void main(String[] args) {
        new QQView().mainMenu();
        System.out.println("客户端退出系统");
    }
}
