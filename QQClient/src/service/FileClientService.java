package service;

import common.Message;
import common.MessageType;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * 服务端 处理文件相关服务的类
 */
public class FileClientService {

    /**
     * 发送文件
     * @param src 本地的文件地址
     * @param dest 发给对方的什么路径下面
     * @param getterId 发给谁
     * @param senderId 谁发的
     */
    public void sendFileTOne(String src, String dest ,String getterId,String senderId) {

        // 组装对象
        Message msg = new Message();
        msg.setMesType(MessageType.MESSAGE_FILE_MES);
        msg.setSender(senderId);
        msg.setGetter(getterId);
        msg.setSrc(src);
        msg.setDest(dest);

        // 需要将文件读取
        FileInputStream fis = null;
        // 创建一个字节数组 长度是你需要传输的那个文件的大小
        byte[] fileBytes = new byte[(int) new File(src).length()];
        // 创建流 进行读取
        try {
            fis = new FileInputStream(src);
            fis.read(fileBytes); // 将src文件读入到程序的字节数组
            //将文件对应的字节数组设置 message
            msg.setFileBytes(fileBytes);

        }catch (Exception e) {
            System.out.println(e.getMessage());
        }finally {
            // 关闭流资源
            try {
                fis.close();
            }catch (Exception e) {
                e.printStackTrace();
            }

        }
        // 提示信息
        System.out.println("\n" + senderId + " 给 " + getterId + " 发送文件 " + src + " 到对方的电脑目录 " +dest);
        // 通过流 将 msg 写入到数据管道
            // 获得线程
        ClientConnectServerThread thread = ManageClientConnectServerThread.getClientConnectServerThread(senderId);
            // 从线程中拿到 socket
        Socket socket = thread.getSocket();
            // 拿到 流对象
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            // 写入
            oos.writeObject(msg);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
