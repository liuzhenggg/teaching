package service;

import java.util.HashMap;

public class ManageClientConnectServerThread {
    // 我们把多个线程 放入到一个 HashMap
    private static HashMap<String, ClientConnectServerThread> hm = new HashMap();

    public static void addClientConnectServerThread(String userID, ClientConnectServerThread clientConnectServerThread) {
        hm.put(userID, clientConnectServerThread);
    }

    // 通过 userId 获取对应线程
    public static ClientConnectServerThread getClientConnectServerThread(String userID) {
        return hm.get(userID);
    }
}

