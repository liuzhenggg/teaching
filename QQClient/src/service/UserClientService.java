package service;

import common.Message;
import common.MessageType;
import common.User;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;

/**
 * 客户端 处理用户 相关业务的服务类
 */
public class UserClientService {

    private User user = new User();

    private Socket socket ;



    /*
        跟据用户信息 到服务端登录
     */
    public boolean checkUser(String userId, String password) {

        // 创建 user 对象
        user.setPassword(password);
        user.setUserId(userId);

        try {
            // 连接到服务端 发送对象信息
            socket = new Socket("127.0.0.1", 9999);
            // 得到 ObjectOutPut 对象
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(user);// 写到 服务端

            // 读取服务端 返回的 Msg 对象 看看到底有没有登录成功
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            Message message = (Message) ois.readObject();

            if (message.getMesType().equals(MessageType.MESSAGE_LOGIN_SUCCEED)) {
                System.out.println("登录成功");
                // 这里是否就要 创建一个线程 和 服务端保持连接 ClientConnectServerThread
                ClientConnectServerThread serverThread = new ClientConnectServerThread(socket);
                serverThread.start();
                // 实际情况是有多个线程要连接服务端 【发消息 发文件 同时】
                // 放到集合中管理
                ManageClientConnectServerThread.addClientConnectServerThread(userId, serverThread);

                return true;
            } else {
                System.out.println("登录失败");// 关闭 连接
                socket.close();
                return false;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return false;

    }

    // 拉取在线用户列表
    public void onLineFriendList() {
        // 发送一个 Message , 类型 为
        // String MESSAGE_GET_ONLINE_FRIEND = "4"; // 要求返回在线用户列表
        Message message = new Message();
        message.setMesType(MessageType.MESSAGE_GET_ONLINE_FRIEND);
        message.setSender(user.getUserId());

        // 发送给服务器
        // 获取到当前线程的Socket 对应的 ObjectOutPutStream 对象
        // 从集合中拿'
        ClientConnectServerThread thread = ManageClientConnectServerThread.getClientConnectServerThread(user.getUserId());
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(thread.getSocket().getOutputStream());
            outputStream.writeObject(message);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }

    // 编写方法 退出客户端 并给服务端发送一个退出系统的 message 对象
    public void logout(){
        Message message = new Message();
        message.setMesType(MessageType.MESSAGE_CLIENT_EXIT);
        // 一定要指定 我是哪个客户端 精准退出
        message.setSender(user.getUserId());

        // 发送Message
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(message);
            System.out.println(user.getUserId() + " 退出了系统");
            // 结束线程
            System.exit(0);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    // 群发消息
    public void sendMessageToAll(String content,String senderId) {
        Message message = new Message();
        message.setMesType(MessageType.MESSAGE_TO_ALL_MES);
        message.setSender(senderId);
        message.setContent(content);
        message.setSentTime(new Date().toString());
        System.out.println(senderId + " 对大家说 " + content);

        // 发送给服务端
        try {
            // 使用当前 userId对应 的线程中的 socket
            ClientConnectServerThread thread = ManageClientConnectServerThread.getClientConnectServerThread(senderId);
            ObjectOutputStream oos = new ObjectOutputStream(thread.getSocket().getOutputStream());
            // 消息回写给服务端
            oos.writeObject(message);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }


    }
}
