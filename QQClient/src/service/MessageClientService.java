package service;

import common.Message;
import common.MessageType;

import java.io.ObjectOutputStream;
import java.util.Date;

/**
 * 该类 是提供和消息相关的服务方法
 */
public class MessageClientService {

    /**
     *
     * @param content 消息内容
     * @param getterId 接收方ID
     */
    public void sendMessageToOne(String content,String senderId,String getterId) {
        // 组装消息类
        Message message = new Message();
         message.setContent(content);
         message.setSender(senderId);
         message.setGetter(getterId);
         message.setSentTime(new Date().toString());
         // 消息类型别忘了 --- 普通的聊天消息
         message.setMesType(MessageType.MESSAGE_COMM_MES);

         // 在客户端提示这句话
        System.out.println(senderId + " 对: " + getterId + " 说: " + content);

        try {
            // 从线程中拿到 socket 再拿到对应的数据管道
          ObjectOutputStream oos =
            new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(senderId).getSocket().getOutputStream());
            // 发送消息
            oos.writeObject(message);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }

    }
}
