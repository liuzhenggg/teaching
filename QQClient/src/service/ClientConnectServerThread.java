package service;

import common.Message;
import common.MessageType;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ClientConnectServerThread extends Thread{
    // 该线程 需要持有 Socket
    private Socket socket;

    // 构造器在构建的时候 可以接收一个 socket 对象
    public ClientConnectServerThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        // 要和服务器保持连接 我们先一直写成true
        while(true){
            try {
                // 读取服务器回送的消息
                System.out.println("客户端的线程，等待读取从服务器端发送的消息");
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                // 返回的一定是 Msg 对象  直接强转
                Message message = (Message) ois.readObject();// 如果服务器没有发送对象 这里会一直阻塞

                // 判断这个 message 的类型然后进行业务处理
                if (message.getMesType().equals(MessageType.MESSAGE_RET_ONLINE_FRIEND)){
                    // 拉取在线用户列表
                    String[] onlineUser = message.getContent().split(" ");
                    System.out.println("======== 当前在线用户列表 =========");
                    for (int i = 0; i < onlineUser.length; i++) {
                        System.out.println("用户: " + onlineUser[i]);
                    }
                }else if (message.getMesType().equals(MessageType.MESSAGE_COMM_MES)){
                    // 私聊 内容直接 在控制台显示
                    System.out.println("\n" + message.getSender() + "对 "
                            + message.getGetter() + " 说：" + message.getContent());
                }else if (message.getMesType().equals(MessageType.MESSAGE_TO_ALL_MES)){
                    // 群发的消息，显示在控制台
                    System.out.println("\n" + message.getSender() + " 对大家说: " + message.getContent());
                }else if (message.getMesType().equals(MessageType.MESSAGE_FILE_MES)){
                    // 接收发送的文件
                    System.out.println("\n"+message.getSender() + " 给我发文件 " + message.getSrc() + " 到我的电脑目录 " + message.getDest());
                    // 取出 message 的文件字节数组，通过文件输出流写到磁盘
                    FileOutputStream fos = new FileOutputStream(message.getDest());
                    // 写入
                    fos.write(message.getFileBytes());
                    // 关闭流
                    fos.close();
                    System.out.println("\n 保存文件成功~~~");

                }
                else {
                    System.out.println("其它业务 暂不处理。。");
                }

            }catch (Exception e){
                System.out.println(e.getMessage());
            }

        }
    }


    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
}
