package servlet.forward;

import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



/**
 * 请求转发
 *
 * 1.请求转发是通过 HttpServletRequest 对象实现的
 * 2.请求转发是服务器内部行为，对客户端是屏蔽的
 * 3.客户端只产生了一次请求，服务端只产生了一对 request 和 response 对象
 * 4.客户端的地址栏是不变的
 * 5.请求的参数是可以继续传递的
 * 6.目标资源可以是 servlet动态资源 也 可以是 html 静态资源
 * 7.目标资源可以是 WEB-INF 下受保护的资源 该方式也是 WEB-INF 下的资源的唯一访问方式
 * 8.目标资源不可以是外部资源
 */

@WebServlet("/ServletA")
public class ServletA extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //  获取请求转发器
        //  转发给servlet  ok
//        RequestDispatcher requestDispatcher = req.getRequestDispatcher("servletB");
        //  转发给一个视图资源 ok
//        RequestDispatcher requestDispatcher = req.getRequestDispatcher("welcome.html");
        //  转发给WEB-INF下的资源  ok
        //RequestDispatcher requestDispatcher = req.getRequestDispatcher("WEB-INF/views/view1.html");
        //  转发给外部资源   no --- 这里找不到的 不是我们项目内部的
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("https://www.baidu.com");

        //  获取请求参数
        String money = req.getParameter("money");
        System.out.println(money);

        //  向请求域中添加数据
        req.setAttribute("reqKey","requestMessage");
        //  做出转发动作
        requestDispatcher.forward(req,resp);
    }


}
