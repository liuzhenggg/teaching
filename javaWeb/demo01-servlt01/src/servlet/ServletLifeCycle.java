package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*

生命周期：
1. 实例化  --- 构造器             第一次请求时
2. 初始化  --- init              构造完毕
3. 接收请求，处理请求  service     每次请求
4. 销毁   destroy               关闭服务  【 注意 servlet 只是处理 HTTP 请求的对象】

    Servlet 在 TomCat 中是单例的  【画图解释】
    servlet 的成员变量在多个线程栈之间是共享的
    所以 不要在 service 方法里面进行修改成员的操作，会引发线程安全问题

类的继承关系：
    1.顶层 servlet 接口 --- 拿出里面的方法讲一下
    2.抽象的类 GenericServlet
    3.HttpServlet

 */

@WebServlet("/servletLifeCycle")
public class ServletLifeCycle  extends HttpServlet {
    public ServletLifeCycle(){
        System.out.println("构造器");
    }

    @Override
    public void init() throws ServletException {
        System.out.println("初始化方法");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("service方法");
    }

    @Override
    public void destroy() {
        System.out.println("销毁方法");
    }
}
