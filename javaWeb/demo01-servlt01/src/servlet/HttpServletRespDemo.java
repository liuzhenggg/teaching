package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 响应报文的相关设置
 */
@WebServlet("/httpServletRespDemo")
public class HttpServletRespDemo extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String info = "<h1>Hello World</h1>";
        // 设置响应头相关的API
        resp.setContentType("text/html"); // 设置响应体里面的数据 的处理格式
        resp.setStatus(200); // 设置响应的状态码
        req.setCharacterEncoding("UTF-8"); // 设置响应体内的数据的编码格式
        resp.setContentLength(info.getBytes().length); // 设置响应体的长度

        // 获得一个向响应体中输入文本字符输出流 并将数据输出到 响应体
        PrintWriter writer = resp.getWriter();
        writer.println(info);

    }

}
