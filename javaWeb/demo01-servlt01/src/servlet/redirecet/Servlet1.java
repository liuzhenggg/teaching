package servlet.redirecet;

import javax.servlet.http.HttpServlet;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 1.响应重定向通过HttpServletResponse对象的sendRedirect方法实现
 * 2.响应重定向是在服务端的提示下，客户端的行为
 * 3.客户端至少发送了两次请求,客户端地址栏是要变化的
 * 4.请求产生了 多次 后端就会有多个 request 对象 此时请求中的参数不能继续自传递
 * 5.目标资源可以是视图资源[html]
 * 6.目标资源不可以是 WEB-INF 下的资源
 */

@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //  获取请求参数
        String username = req.getParameter("username");
        System.out.println(username);
        //  向请求域中添加数据
        req.setAttribute("reqKey","requestMessage");
        //  响应重定向
        // 重定向到servlet动态资源 OK
//        resp.sendRedirect("servlet2");
        // 重定向到视图静态资源 OK
        //resp.sendRedirect("welcome.html");
        // 重定向到WEB-INF下的资源 NO
        //resp.sendRedirect("WEB-INF/views/view1");
        // 重定向到外部资源 -- 这里是OK的
        resp.sendRedirect("https://www.baidu.com");
    }

}
