package servlet;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/*
servlet 开发流程
1.创建 javaWeb 项目 同时添加 tomCat 依赖
2.重写 service 方法
3.在 service 方法中，定义业务处理代码
4.在 web.xml 中，配置 servlet 对应的请求映射路径

HttpServlet 带着大家看一下 这个类的所在位置

Content-Type 响应头的问题
    MIME 类型响应头 媒体类型 文件类型 响应的数据类型
    MIME 类型用于告诉客户端响应的数据是什么类型的数据，客户端以此类型决定用什么方式解析响应体

//   最后 图4 和图3 是一样的 用4 解释一下 路径问题


 */
// implements Servlet 这样有问题 要重写很多用不上的方法
public class UserServlet  extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 1. 从 request 对象中获取 请求中的任何信息
        String username = req.getParameter("userName");
        // 2. 处理业务代码
            // 2.1 <h1>YES<h1> 没有 Content-Type 就当 html 处理
        String info = "<h1>YES<h1>";
        if ("root".equals(username)) {
            info = "NO";
        }
        // 3. 将要响应的数据放入 response
        resp.getWriter().println(info);
        // 配置路径


//        resp.setHeader("Content-type", "text/css");
    }

}
