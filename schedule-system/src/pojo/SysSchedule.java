package pojo;
/**
 * 数据库映射 实体类
 * 逻辑删除
 *  实际开发过程中 不去真的删除数据库里面的数据 而是给一个删除标识  0 代表以删除 1 代表未删除
 *
 */
public class SysSchedule {

    // 日程编号
    private int id;

    // 日程的内容
    private String schedule_context;

    // 日程的进度
    private String schedule_progress;

    // 日程所属的用户是谁
    private String schedule_user_name;

    private String is_delete;

    public SysSchedule(int id, String schedule_context, String schedule_progress, String schedule_user_name) {
        this.id = id;
        this.schedule_context = schedule_context;
        this.schedule_progress = schedule_progress;
        this.schedule_user_name = schedule_user_name;
    }

    public SysSchedule() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSchedule_context() {
        return schedule_context;
    }

    public void setSchedule_context(String schedule_context) {
        this.schedule_context = schedule_context;
    }

    public String getSchedule_progress() {
        return schedule_progress;
    }

    public void setSchedule_progress(String schedule_progress) {
        this.schedule_progress = schedule_progress;
    }

    public String getSchedule_user_name() {
        return schedule_user_name;
    }

    public void setSchedule_user_name(String schedule_user_name) {
        this.schedule_user_name = schedule_user_name;
    }

    public String getIs_delete() {
        return is_delete;
    }

    public void setIs_delete(String is_delete) {
        this.is_delete = is_delete;
    }
}
