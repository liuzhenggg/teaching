package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import pojo.Result;
import pojo.SysSchedule;
import pojo.SysUser;
import service.ScheduleService;
import service.impl.ScheduleServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 这个 servlet 的作用是 对 日程的 相关操作
 */
@WebServlet("/schedule/*")
public class ScheduleController extends HttpServlet {
    // 创建服务层对象 使用接口接受对象
    private ScheduleService scheduleService = new ScheduleServiceImpl();
    // 增删改查 CRUD
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 跟据请求路径 去 确定执行哪个方法
        req.setCharacterEncoding("utf-8");
        String requestURI = req.getRequestURI();
        String[] split = requestURI.split("/");
        String methodName = split[split.length - 1];
        if (methodName.equals("querySchedule")) {
            // 1. 查询当前登录用户的 所有日程信息
            querySchedule(req, resp);
        }else if (methodName.equals("deleteScheduleById")) {
            // 2. 跟据日程的 ID 去 删除 日程信息

        }else if (methodName.equals("editScheduleById")) {
            // 3. 跟据日程的 ID 去 修改 日程信息

        }else if (methodName.equals("addSchedule")) {
            // 4. 添加日程信息 到数据库
        }else if (methodName.equals("addDefaultSchedule")) {
            addDefaultSchedule(req, resp);
        }else if (methodName.equals("updateSchedule")) {
            // 修改 日程信息
            updateItem(req, resp);

        }
    }

    public void addDefaultSchedule(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        //  调用服务层方法,为当前用户新增一个默认空数据
        int i = scheduleService.addDefault(username);
        Result result;
        if (i > 0) {
            result = new Result("200", "添加成功！", null);
        }else {
            result = new Result("990", "添加失败！", null);
        }
        ObjectMapper mapper = new ObjectMapper();
        // 转换成为 JSON 返回前端
        String json = mapper.writeValueAsString(result);
        resp.getWriter().write(json);
    }

    // 查询日程
    public void querySchedule(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 1. 获取当前登录的是哪个用户 从会话域里面拿用户信息
        HttpSession session = req.getSession();
        SysUser user = (SysUser) session.getAttribute("user");
        String username = req.getParameter("username");
        // 2.获取用户的 用户名
//        String userName = "刘政";
        // 3.要跟据用户名查找数据库 获得当前用户 的所有日程信息
        List<SysSchedule> scheduleList = scheduleService.querySchedule(username);
        // 这里先循环打印看一下
        for (SysSchedule sysSchedule : scheduleList) {
            System.out.println(sysSchedule);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("itemList", scheduleList);

        // 转换成为 JSON
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(data);
        resp.setContentType("application/json");
        resp.setCharacterEncoding("utf-8");
        resp.getWriter().write(json);
    }

    public void updateItem(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        StringBuffer sb = transFor(req);
        ObjectMapper mapper = new ObjectMapper();
        SysSchedule sysSchedule = mapper.readValue(sb.toString(), SysSchedule.class);
        System.out.println(sysSchedule.toString());
    }

    public StringBuffer transFor(HttpServletRequest req) throws IOException {
        // 1. 接收客户端提交的 json 参数 并转化为 User 对象
        BufferedReader reader = req.getReader();
        StringBuffer sb = new StringBuffer();
        String line = reader.readLine();
        while (line != null) {
            sb.append(line);
            line = reader.readLine();
        }
        return sb;
    }
}
