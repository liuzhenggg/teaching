package dao;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * 德鲁伊连接池 配置  --- 只用写一遍
 */
public class DruidConfig {

    /**
     * 返回连接对象
     * @return 连接对象
     * 直接将数据库的 相关信息 写到代码里面好不好？
     * 行业术语：直接将 配置写到代码里面 称为 硬编码 <不推荐，不利于后期修改维护></>
     *  将配置 写到配置文件里面 称为 软编码 大家上班之后 百分之九十九你都是使用软编码
     */
    public static Connection getConnection() throws SQLException {
        // 1. 创建 德鲁伊资源对象
        DruidDataSource ds = new DruidDataSource();
        // 1. 加载 数据库连接 驱动
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        // 2. 配置数据库连接
        ds.setUrl("jdbc:mysql://127.0.0.1:3306/test");
        // 3. 配置用户名
        ds.setUsername("root");
        // 4. 配置密码
        ds.setPassword("root");
        // 非必要配置
        ds.setInitialSize(10);// 连接池中初始化 有多少连接对象
        ds.setMaxActive(50); // 连接池 最大 连接对象 有 50 个
        return ds.getConnection();
    }

    // 使用软编码 修改
    // 好处：将数据库的配置 写到配置文件后续 如果更换了数据库 只需要修改配置 而不用动代码
    public static Connection getConnection2() throws Exception {
        // 1. 读取 数据库配置  --- 使用 字节输入流 将 配置文件 加载到程序 【先使用反射获取字节码文件 在使用流读取】
        InputStream is = DruidConfig.class.getClassLoader().getResourceAsStream("db.properties");
        Properties prop = new Properties();
        // 2.将 读取好的配置文件 加载到 proper 对象
        prop.load(is);
        // 3. 创建 德鲁伊连接池资源对象
        DataSource ds = DruidDataSourceFactory.createDataSource(prop);
        return ds.getConnection();
    }
}
