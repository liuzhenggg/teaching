package dao.impl;

import dao.BaseDao;
import dao.DruidConfig;
import dao.ScheduleDao;
import pojo.SysSchedule;
import pojo.SysUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ScheduleDaoImpl extends BaseDao implements ScheduleDao {


    @Override
    public List<SysSchedule> querySchedule(String userName) {

        // 根据服务层 传入的参数 去操作数据库
       List<SysSchedule> list = new ArrayList<SysSchedule>();
        try {
            //  连接数据库 并查询数据
            Connection connection = DruidConfig.getConnection();
            // sql 语句
            PreparedStatement prepared = connection.prepareStatement("select * from sys_schedule where schedule_user_name = ? and is_delete = 1");
            // 设置参数 将第一个 ？ 处的参数 设置为 传进来的 userName
            prepared.setString(1, userName);
            // 执行 sql 获得 数据库 返回的结果
            ResultSet resultSet = prepared.executeQuery();
            // 将查出来的结果 循环封装为 一个对象
            while (resultSet.next()) {
                SysSchedule schedule = new SysSchedule();
                schedule.setId(resultSet.getInt("id"));
                schedule.setSchedule_context(resultSet.getString("schedule_context"));
                schedule.setSchedule_progress(resultSet.getString("schedule_progress"));
                schedule.setSchedule_user_name(resultSet.getString("schedule_user_name"));
                list.add(schedule);
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return list;
    }

    @Override
    public int addDefault(String userName) {
        try {
            //  连接数据库 并查询数据
            Connection connection = DruidConfig.getConnection();
            // sql 语句
            PreparedStatement prepared = connection.prepareStatement("insert into sys_schedule (schedule_user_name,is_delete) values(?,?)");
            // 设置参数 将第一个 ？ 处的参数 设置为 传进来的 userName
            prepared.setString(1, userName);
            prepared.setString(2, "1");
            // 执行 sql 获得 数据库 返回的结果
            int i = prepared.executeUpdate();
            // 5. 释放资源
            connection.close();
            System.out.println("新增日程信息成功！！！");
            return i;
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

}
