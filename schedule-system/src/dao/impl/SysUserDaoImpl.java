package dao.impl;


import dao.BaseDao;
import dao.DruidConfig;
import dao.SysUserDao;
import pojo.SysSchedule;
import pojo.SysUser;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class SysUserDaoImpl extends BaseDao implements SysUserDao {

    @Override
    public int addSysUser(SysUser sysUser) {
        String sql ="insert into sys_user values(DEFAULT,?,?)";
        int i = executeUpdate(sql, sysUser.getUsername(), sysUser.getUserPwd());
        return i;
    }

    @Override
    public SysUser findByUsername(String username) {
        SysUser sysUser = new SysUser();
        try {
            //  连接数据库 并查询数据
            Connection connection = DruidConfig.getConnection();
            // sql 语句
            PreparedStatement prepared = connection.prepareStatement("select * from sys_user where user_name = ? ");
            // 设置参数 将第一个 ？ 处的参数 设置为 传进来的 userName
            prepared.setString(1, username);
            // 执行 sql 获得 数据库 返回的结果
            ResultSet resultSet = prepared.executeQuery();
            // 将查出来的结果 循环封装为 一个对象
            while (resultSet.next()) {
                sysUser.setUsername(resultSet.getString("user_name"));
                sysUser.setUserPwd(resultSet.getString("user_pwd"));
                return sysUser;
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return sysUser;
    }
}
