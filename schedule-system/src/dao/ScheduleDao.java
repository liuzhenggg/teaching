package dao;

import pojo.SysSchedule;

import java.util.List;

public interface ScheduleDao {

    List<SysSchedule> querySchedule(String userName);

   int addDefault(String userName);
}
