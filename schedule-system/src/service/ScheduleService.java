package service;

import pojo.SysSchedule;

import java.util.List;

/**
 * 日程相关的服务接口
 */
public interface ScheduleService {

    // 查询
    List<SysSchedule> querySchedule(String userName);

    int addDefault(String userName);
}
