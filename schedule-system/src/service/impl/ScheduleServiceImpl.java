package service.impl;

import dao.ScheduleDao;
import dao.impl.ScheduleDaoImpl;
import pojo.SysSchedule;
import service.ScheduleService;

import java.util.Collections;
import java.util.List;

/**
 * 日程管理的 服务类
 */
public class ScheduleServiceImpl implements ScheduleService {

    ScheduleDao scheduleDao = new ScheduleDaoImpl();

    @Override
    public List<SysSchedule> querySchedule(String userName) {
       return scheduleDao.querySchedule(userName);

    }

    @Override
    public int addDefault(String userName) {
        return scheduleDao.addDefault(userName);
    }
}
