package io.transformation;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 演示 使用 InputStreamReader 转换流解决中文乱码问题
 * 将字节流 FileInPutStream 转换成支付流 InputStreamReader 指定编码 gbk/utf-8
 */
public class InputStreamReader_ {
    public static void main(String[] args) throws IOException {
        String filePath = "e:\\hello.txt";

        // 解读
        // 1. 把 FileInputStream 转换 InputStreamReader
        // 2. 指定编码格式 gbk 【进行读取】
        InputStreamReader isr = new InputStreamReader(new FileInputStream(filePath),"gbk");
        // 3. 把 InputStreamReader 传入 BufferedReader  【将其 包装 转换】
        BufferedReader br = new BufferedReader(isr);
        // 4. 读取
        String s = br.readLine();
        System.out.println("读取内容=" + s);

        // 关闭流
        br.close();
    }
}
