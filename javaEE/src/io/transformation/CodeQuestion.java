package io.transformation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *  转换流
 */
public class CodeQuestion {
    public static void main(String[] args) throws IOException {
        String filePath = "e:\\hello.txt";
        BufferedReader br = new BufferedReader(new FileReader(filePath));

        System.out.println("读取到的内容：" + br.readLine());
        br.close();

        // 记事本另存为 编码格式改成 ANIS 就会出问题
    }


}
