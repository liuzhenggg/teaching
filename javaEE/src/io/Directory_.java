package io;

import org.junit.Test;

import java.io.File;
import java.io.InputStream;

// 目录操作
public class Directory_ {
    public static void main(String[] args) {

    }

    // 判断 一个文件是否存在，如果存在则删除对应文件
    @Test
    public void m1(){
        String filePath = "e:\\news1.txt";
        File file = new File(filePath);
        if(file.exists()){
            if(file.delete()){
                System.out.println("删除成功");
            }else {
                System.out.println("删除失败");
            }
        }else {
            System.out.println("文件不存在...");
        }
    }

    //这里我们需要体会到，在java编程中，目录也被当做文件
    // 需要注意的是，目录里面不能存在 文件 或 子目录 否则会删除失败
    @Test
    public void m2() {
        String filePath = "e:\\demo01";
        File file = new File(filePath);
        if(file.exists()){
            if(file.delete()){
                System.out.println("目录删除成功");
            }else {
                System.out.println("目录删除失败");
            }
        }else {
            System.out.println("该目录不存在...");
        }
    }

    @Test
    public void m3(){
        String filePath = "e:\\demo02";
        File file = new File(filePath);
        if(file.exists()){
            System.out.println("文件夹已存在");
        }else {
            // mkdir 是 创建一级目录  后面加S 则可创建多级目录
            if (file.mkdirs()) {
                System.out.println("文件夹创建成功");
            }else {
                System.out.println("文件夹创建失败");
            }
        }
    }
}
