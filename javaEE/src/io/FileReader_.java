package io;

import org.junit.Test;

import java.io.FileReader;
import java.io.IOException;

/**
 * 字符输入流
 */
public class FileReader_ {
    public static void main(String[] args) {

    }

    // 一次读取一个字符
    @Test
    public void f1(){
        // 1. 创建流对象
        FileReader fr = null;
        try {
            fr = new FileReader("e:\\news1.txt");

            // 读取
            // read: 每次读取单个字符，如果遇到文件末尾，返回-1
            int date =0;
            while((date=fr.read())!=-1){
                System.out.print((char)date);
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            // 使用完毕后关掉这个流
            try {
               if (fr != null) {
                   fr.close();
               }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }
    }

    // 一次读取多个字符 --- 效率更高一些
    @Test
    public void f2(){
        // 1. 创建流对象
        FileReader fr = null;
        try {
            fr = new FileReader("e:\\news1.txt");

            // 读取
            // read: 每次读取单个字符，如果遇到文件末尾，返回-1
            char[] buff = new char[8]; // 一次读取 1024个字符
            int len = 0;
            while((len=fr.read(buff))!=-1){
                System.out.println(new String(buff,0,len));
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            // 使用完毕后关掉这个流
            try {
                if (fr != null) {
                    fr.close();
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
