package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopy {
    public static void main(String[] args) {
        // 完成 文件拷贝， 将 e:\\风景.jpg 拷贝到 d:\\
        // 思路分析
        // 1. 创建文件的输入流, 将文件读入到程序 中
        // 2. 创建文件的输出流, 将读取到的文件数据 写入到指定的文件

        String filePath = "e:\\风景.jpg";
        String srcFilePath = "e:\\风景2.jpg";
        FileInputStream fis = null;
        FileOutputStream fos = null;

        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(filePath);
            fos = new FileOutputStream(srcFilePath);
            // 一个一个字节的读取实在太慢 我们一次 读取 1024 个字节
            while (fis.read(buffer) != -1) {
                // 如果图片 打不开 那就是读取写入不完整 需要  fos.write(buffer, 0, buffer.length); 这个方法
                fos.write(buffer);
            }
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                assert fis != null;
                fis.close();
                assert fos != null;
                fos.close();
            }catch (IOException e){
                System.out.println(e.getMessage());
            }

        }

    }


}
