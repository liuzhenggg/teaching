package io.work;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Work2 {
    public static void main(String[] args) throws IOException {
        String filePath = "e:\\a.txt";
        String line = null;
        int lineNum = 0;
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        // readLine 是读一行
        while ((line = br.readLine()) != null) { // 循环读取
            System.out.println(++lineNum + line);
        }
        br.close();
    }
}
