package io.work;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Work3 {
    public static void main(String[] args) throws IOException {

        String filePath = "C:\\Users\\Mr.Liu\\Desktop\\codeT\\teaching\\javaEE\\src\\dog.properties";
        Properties prop = new Properties();
        prop.load(new FileReader(filePath));
        String name = prop.getProperty("name");
        int age = Integer.parseInt(prop.getProperty("age"));
        String color = prop.getProperty("color");

        Dog dog = new Dog(name, age, color);
        System.out.println(dog.getName());
        System.out.println(dog.getAge());
        System.out.println(dog.getColor());
    }
}

class Dog {
   private String name;
   private int age;
   private String color;

    public Dog(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}