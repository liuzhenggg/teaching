package io.work;

import java.io.*;

public class work1 {
    public static void main(String[] args) throws IOException {
        String directoryPath = "e:\\mytemp";
        File file = new File(directoryPath);
        if (!file.exists()) {
            // 创建
            if (file.mkdir()) {
                System.out.println("创建 " + directoryPath + "成功");
            }else {
                System.out.println("失败");
            }
        }
        String filePath = directoryPath + "\\hello.txt";
        file = new File(filePath);
        if (!file.exists()) {
            // 不存在则创建文件
            if (file.createNewFile()) {
                System.out.println(filePath + "创建成功~");
                // 如果文件存在，我们就使用 BufferedWriter 字符输入流写入内容
                BufferedWriter bw =new BufferedWriter(new FileWriter(file));
                bw.write("hello,word~~ 终于降温了");
                bw.close();
            }else {
                System.out.println("创建失败");
            }
        }else {
            System.out.println(filePath + "已存在");
        }
    }
}
