package io;



import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FileCreate {

    public static void main(String[] args) throws IOException {

    }


    // 注意 要选 5 以上
    @Test
    public void create01() throws IOException {
        // 方式 3
        // 此时 第一个 / 表示转义字符
        File file = new File("d:\\");
        // 这里只是创建了对象而已
        File newFile = new File(file,"news3.txt");

        // newFile.createNewFile() 执行了这一行才是真正的创建了文件
        if (newFile.createNewFile()){
            System.out.println("创建ok");
        }
    }

    @Test
    public void info(){
        File file = new File("e:\\news1.txt");
        try {
            boolean newFile = file.createNewFile();
            // 调用相应的方法，得到对应信息
            System.out.println("文件名 = " + file.getName());
            System.out.println("文件的绝对路径 = " + file.getAbsolutePath());
            System.out.println("文件父级目录 = " + file.getParent());
            System.out.println("文件大小 = " + file.length()); // 以字节为大小
            // 一般情况下有两种编码格式  utf-8
            // 你好
            // win 的编码格式是 GBK

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

}
