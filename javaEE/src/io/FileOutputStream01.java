package io;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStream01 {
    public static void main(String[] args) {

    }


    // 使用 File Output Stream 在 a.txt 文件 中写入  “hello , world”, 如果文件 a.txt 不存在则会先创建文件
    @Test
    public void writeFile() {
        // 创建 FileOutputStream 对象
        String filePath = "e:\\a.txt";
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filePath);
            fos.write("hello,word".getBytes());

        }catch (IOException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                assert fos != null;
                fos.close();
            }catch (IOException e) {
                System.out.println(e.getMessage());
            }

        }

    }


}
