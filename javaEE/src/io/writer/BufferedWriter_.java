package io.writer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * 字符缓冲流 --- 没有前缀 因为都包进去了
 */
public class BufferedWriter_ {
    public static void main(String[] args) throws IOException {
        String filePath = "e:\\ok.txt";

        BufferedWriter bw = new BufferedWriter(new FileWriter(filePath));
        bw.write("hello");
        bw.write("今天天气很炎热！！！");

        bw.close();
    }
}
