package io;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * 演示 FileInputStream 的使用（字节输入流 文件 --> 程序）
 */
public class FileInputStream01 {
    public static void main(String[] args) {

    }

    @Test
    public void  readFile01() {
        String filePath = "e:\\hello.txt";
        FileInputStream fis = null;
        int readData = 0;
        try {
            // 创建 fis 对象 用于读取 文件
            fis = new FileInputStream(filePath);
            // 从该输入流读取一个字节的数据。如果返回 -1 则表示读取完毕

            // 在 utf-8 的编码格式下 一个中文汉字占三个字节 这样一字节一个字节的读取输出 百分比会乱码
            // 文本文件 最好用字符流来处理
             while ((readData = fis.read()) != -1) {
                 System.out.print((char) readData);
             }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }finally {
            // 关闭文件流 防止资源浪费
            try {
                assert fis != null;
                fis.close();
            }catch (IOException e){
                System.out.println(e.getMessage());
            }

        }

    }
}
