package io;

import java.io.*;

public class BufferedCopy_ {

    public static void main(String[] args) throws IOException {
        String src = "e:\\a.txt";
        String src2 = "e:\\a2.txt";

        // 注意：因为Reader 和 Writer 是 按字符读取的，因此不能操作二进制文件（字节文件），比如图片 ，音乐 会造成读取数据错误
        BufferedReader br = new BufferedReader(new FileReader(src));
        BufferedWriter bw = new BufferedWriter(new FileWriter(src2));

        String line;
        while ((line = br.readLine()) != null) {
            bw.write(line);
            // 换行
            bw.newLine();
        }

        br.close();
        bw.close();
    }
}
