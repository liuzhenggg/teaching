package io.reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferedReader_ {
    public static void main(String[] args) throws IOException {

        String filePath = "e:\\a.java";
        // 创建 bufferedReader

        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        // 读取
        // 说明
        // 1. bufferedReadLine() 是按行读取文件
        // 2. 当返回 null 时，表示文件读取完毕
       String line = "";
       while ((line = bufferedReader.readLine()) != null) {
           System.out.println(line);
       }

       // 关闭流
        bufferedReader.close();
    }
}
