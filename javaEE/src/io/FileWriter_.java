package io;

import org.junit.Test;

import java.io.FileWriter;

/**
 * 字符 输出流
 */
public class FileWriter_ {
    public static void main(String[] args) {

    }

    @Test
    public void f1() {
        FileWriter fw = null;
        try {
            // 创建字符输出流对象
            fw = new FileWriter("e:\\story.txt");
            // 写入数据
            // write(): 写入单个字符
            fw.write('H'); // 注意: 如果这个流你不进行关闭则内容不会写进去 -- 工作白干
            // writer(char[]): 写入指定数组
            char[] chars = {'a', 'b', 'c', 'd', 'e', 'f'};
            fw.write(chars);
            // writer(char[],off , len): 写入字符指定数组的指定部分
            fw.write("今年开学天气很热".toCharArray(), 0, 2);
            // writer(String)：写入整个字符串
            // writer(String, off, len): 写入字符串的指定部分
            fw.write("今年开学天气很热", 0,5);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            try {
                if (fw != null) {
                    fw.close();
                }
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }
}
