package io.properties;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Properties01 {
    public static void main(String[] args) throws IOException {
        // 传统方法读取
        BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Mr.Liu\\Desktop\\codeT\\teaching\\javaEE\\src\\mysql.properties"));
        String line = "";
        while ((line = br.readLine()) != null) {
            String[] split = line.split("=");
            System.out.println(split[0] + "值是:" +split[1]);
        }

        br.close();

        // 存在的问题： 不好判断 哪个键对应的哪个值
            // 2. 单个的值不好取
    }
}
