package io.properties;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Properties02 {
    public static void main(String[] args)  throws IOException {
        // 1. 使用 properties 类来读取 mysql 的配置文件

        // 1. 创建 properties 对象
        Properties prop = new Properties();
        // 2. 加载配置
        prop.load(new FileReader("C:\\Users\\Mr.Liu\\Desktop\\codeT\\teaching\\javaEE\\src\\mysql.properties"));
        // 3.把 k-v 显示到控制台
        prop.list(System.out);
        // 4. 根据 key 获取对应的值
        String user = prop.getProperty("user");
        String pwd = prop.getProperty("pwd");
        System.out.println("user = " + user);
        System.out.println("pwd = " + pwd);
    }

    // 2题 添加配置文件
    @Test
    public void m2() throws IOException {
        Properties prop = new Properties();
        prop.setProperty("user", "汤姆"); // 注意 保存是 是中文的 unicode 编码值
        prop.setProperty("pwd", "123456");

        // 将 k-v 转存文件中即可
        prop.store(new FileOutputStream("C:\\Users\\Mr.Liu\\Desktop\\codeT\\teaching\\javaEE\\src\\mysql2.properties"),null);


        System.out.println("保存配置文件成功");
    }
}
