package io;

import java.io.*;

/**
 * 字节缓冲流
 */
public class BufferedCopy02 {
    public static void main(String[] args) throws IOException {
        String src = "e:\\风景.jpg";
        String src2 = "e:\\风景2.jpg";

        // 以字节为单位 --- 来操作文件
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(src));
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(src2));

        byte[] buf = new byte[1024];
        int readLen = 0;
        while ((readLen = bis.read(buf)) != -1) {
            bos.write(buf, 0, readLen);
        }

        // 关闭流
        bis.close();
        bos.close();

    }


}
