package io.modifier;

/**
 * 将这个类 做成 处理流/包装流
 */
public class BufferedReader_ extends Reader_{

    //私有 属性 属性是 Reader_ 类型
    private Reader_ reader_;

    // 核心是这个构造器 ！！！ 还有多态的知识点 接收子类
    public BufferedReader_(Reader_ reader_) {
        this.reader_ = reader_;
    }

    // 让方法更加的灵活，假如我想多次读取文件
    // 没动 FileReader_ 里面的  readFile 代码 却对其进行了增强
    public void  readFiles(int num) {
        for (int i = 0; i < num; i++) {
            reader_.readFile();
        }
    }

    // 还可以对 String 内的方法进行读取
}
