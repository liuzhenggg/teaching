package io.modifier;

/**
 * 实现
 */
public class Test_ {
    public static void main(String[] args) {
        // 因为处理流 内部封装了 节点流
        BufferedReader_ br = new BufferedReader_(new FileReader_());

        // 读 一次  【文件】
        br.readFile();

        // 读多次
        br.readFiles(100);

        // 还可以 对 String 进行读取

     }
}
