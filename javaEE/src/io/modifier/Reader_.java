package io.modifier;

/**
 * 模拟 修饰器设计模式
 */
public abstract class Reader_ { // 抽象类

    public void readFile() {}

    public void readerString(){}

}
