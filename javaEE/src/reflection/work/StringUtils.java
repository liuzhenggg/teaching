package reflection.work;

import reflection.Example05.Example05;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class StringUtils {

    public String toString(Object obj) {
        // 获取这个对象所对应的类的字节码文件对象
        Class clazz = obj.getClass();

        // 创建 缓冲字符串对象 用以拼接 类的信息
        StringBuilder str = new StringBuilder();

        // 获得类所在的包
        Package packageName = clazz.getPackage();

        // 获取包的相关信息
        str.append("包名：" + packageName.getName() + "\n");

        // 获得类的简单名称
        String simpleName = clazz.getSimpleName();
        // clazz.getSimpleName() 类的简写名称 （不包含路径）
        // clazz.getName 得到类的名称 （包含路径）
        str.append("类名：" + simpleName + "\n");
        str.append("公共的构造方法：\n");

        // 获取类的公共构造的方法
        // 获得一个类的所有构造方法
        Constructor[] constructors = clazz.getDeclaredConstructors();
        // 循环打印 构造方法的信息
        for (Constructor constructor : constructors) {
            str.append(constructor.toGenericString() + "\t");
        }

        // 获得类的成员信息
        str.append("类的属性信息: \n");
        Field fields[] = clazz.getDeclaredFields();
        for (Field field : fields) {
            str.append(field.getName() + "\t");
        }

        // 获取类的方法信息
        str.append("类的方法信息: \n");
        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            str.append(method.toGenericString() + "\n");
        }

        // 返回
        return str.toString();

    }

    public static void main(String[] args) {
        StringUtils stringUtils = new StringUtils();
        System.out.println(stringUtils.toString(new Object()));
    }
}
