package reflection.work.w02;

import java.util.Scanner;

public class Work02 {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你要使用的交通工具名称：");
        String choice = sc.nextLine();
        // 路径
        String transport = "reflection.work.w02." + choice;

        // 定义计算速度的相关参数
        double a = 23, b = 24, c = 25;

        // 通过反射获取指定的交通工具所对应的类的字节码文件对象
        Common common = (Common) Class.forName(transport).newInstance();

        // 计算速度
        System.out.println(choice + "speed is :" + common.getSpeed(a, b, c));

    }
}
