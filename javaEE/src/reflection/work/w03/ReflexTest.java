package reflection.work.w03;

import java.io.FileReader;
import java.lang.reflect.Method;
import java.util.Properties;

public class ReflexTest {
    public static void main(String[] args) throws Exception {
        // 加载配置文件里面的内容 根据配置文件里面写的类去执行‘
        Properties properties = new Properties();
        properties.load(new FileReader("C:\\Users\\Mr.Liu\\Desktop\\codeT\\teaching\\javaEE\\src\\reflection\\work\\w03\\test.properties"));

        // 从集合中获取对应的数据
        String className = properties.getProperty("className");
        String methodName = properties.getProperty("methodName");

        // 获取该类所对应的字节码文件对象
        Class clazz = Class.forName(className);

        // 通过反射创建该类的对象
        Object obj = clazz.newInstance();

        // 获取这个对象的 指定方法信息
        Method method = clazz.getMethod(methodName);

        // 通过对象执行方法 --- 注意这里的语法得着重讲
        method.invoke(obj); // 通过 obj 调用指定的方法

    }
}
