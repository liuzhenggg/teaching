package reflection.example06;

import java.lang.reflect.Method;

interface China{
    public void sayChina();
}
class Person implements China{
    private String name;
    private int age;
    public Person(String name,int age){
        this.setName(name);
        this.setAge(age);
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String toString() {
        return "姓名："+this.name+",年龄："+this.age;
    }

    @Override
    public void sayChina() {

    }
}
class Example06{
    public static void main(String args[]) throws Exception{
        Class clazz = null;

        clazz = Class.forName("reflection.example06.Person");

      Method[] methods = clazz.getMethods();
        for (int i = 0;i < methods.length; i++){
            System.out.println("方法："+ methods[i].getName());
        }
    }
}
