package threaduse;

/**
 * 三个窗口售票
 */
public class SellTicket {
    public static void main(String[] args) {
        SellTicket01 sellTicket = new SellTicket01();
        SellTicket01 sellTicket1 = new SellTicket01();
        SellTicket01 sellTicket2 = new SellTicket01();

        sellTicket.start();// 启动售票线程
        sellTicket1.start();// 启动售票线程
        sellTicket2.start();// 启动售票线程

//        System.out.println("====使用接口方式====");
//        SellTicket02 sellTicket3 = new SellTicket02();
//        new Thread(sellTicket3).start();
//        new Thread(sellTicket3).start();
//        new Thread(sellTicket3).start();
    }
}


class SellTicket01 extends Thread {
    private static int ticketNum = 100; // 线程共享

    @Override
    public void run() {
        while (true){
            if (ticketNum <= 0){
                System.out.println("售票结束.....");
                break;
            }
            // 休眠 50 毫秒
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("窗口 " + Thread.currentThread().getName() + " 售出一张票 " + " 剩余票数= " + (ticketNum--));
        }
    }

}

// 使用接口的方式
class SellTicket02 implements Runnable {
    private int ticketNum = 100; // 是同一个对象 所以不用静态

    @Override
    public void run() {
        while (true){
            if (ticketNum <= 0){
                System.out.println("售票结束.....");
                break;
            }
            // 休眠 50 毫秒
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println("窗口 " + Thread.currentThread().getName() + " 售出一张票 " + " 剩余票数= " + (ticketNum--));
        }
    }

}