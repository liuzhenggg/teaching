package threaduse;

/**
 * 查看线程的状态
 */
public class ThreadState_ {
    public static void main(String[] args) throws InterruptedException {
        T4 t = new T4();
        System.out.println(t.getName() + "状态 " + t.getState());
        t.start();

        // 如果不是终止状态 则查看线程状态 --- 只要线程没有终止 我就不停的查看状态
        while (Thread.State.TERMINATED != t.getState()) {
            System.out.println(t.getName() + " 状态 " + t.getState());
            Thread.sleep(500); // 主线程休眠
        }

        System.out.println(t.getName() + " 状态 " + t.getState());
    }
}

class T4 extends Thread {
    @Override
    public void run() {
        while (true) {
            for (int i = 0; i < 10; i++) {
                try {
                    System.out.println("hi " + i);
                    Thread.sleep(1000); // 休眠等待
                }catch (Exception e) {
                    System.out.println(e.getMessage());
                }

            }
            break;
        }
    }
}
