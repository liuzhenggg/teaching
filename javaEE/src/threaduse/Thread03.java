package threaduse;

/**
 * 请编写一个程序，创建两个线程，一个线程每隔1秒输出 “hello，world”
 * 输出10次 退出，一个线程每隔1秒输出 “hi”, 输出5次退出
 */
public class Thread03 {
    public static void main(String[] args) {
        T1 t1 = new T1();
        T2 t2 = new T2();
        Thread thread1 = new Thread(t1);
        Thread thread2 = new Thread(t2);
        thread1.start();
        thread2.start();

        System.out.println("主线程执行完毕~~~");
    }
}

class T1 implements Runnable {
    int count = 1;
    @Override
    public void run() {
        while (true){
            try {
                System.out.println("hello,word~~ " + count++ +" " + Thread.currentThread().getName());
                Thread.sleep(1000);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
            if (count >= 10){
                break;
            }
        }

    }
}

class T2 implements Runnable {
    int count = 1;
    @Override
    public void run() {
        while (true){
            try {
                System.out.println("hi " + count++ +" " + Thread.currentThread().getName());
                Thread.sleep(1000);
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
            if (count >= 5){
                break;
            }
        }

    }
}