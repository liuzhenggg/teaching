package threaduse.method;

/**
 *
 */
public class ThreadMethod02 {
    public static void main(String[] args) throws InterruptedException {
        T2 t2 = new T2();
        t2.start();

        for (int i = 0; i < 20; i++) {
            Thread.sleep(1000);
            System.out.println("主线程(小弟) 吃了 " + i + "包子");
            if (i == 5) {
                System.out.println("主线程(小弟) 让 子线程(老大) 先吃");
                t2.join();// 让 t2 线程插队

                // 阶段 二 尝试礼让代码
                // Thread.yield();

                System.out.println("老大吃完了，小弟接着吃~~~~");
            }
        }
    }
}

class T2 extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(1000);// 休眠一秒
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
            System.out.println("子线程 吃了 " + i + " 包子");
        }
    }
}
