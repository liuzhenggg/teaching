package threaduse.method;

/**
 * 线程的常用方法演示
 */
public class ThreadMethod01 {
    public static void main(String[] args) throws InterruptedException {
        // 测试相关的方法
        T t = new T();
        t.setName("老刘");
        t.setPriority(Thread.MIN_PRIORITY);// 设置线程的优先级
        t.start();// 启动子线程

        // 主线程打印 5 hi,然后我就中断 子线程的休眠
        for (int i = 0; i < 5; i++) {
            Thread.sleep(1000);
            System.out.println("hi "+ i);
        }

        // 提前中断 t 线程 --- 注意： 是中断 不是终止 分析运行结果
        t.interrupt();
        System.out.println(t.getPriority()); // 获取线程优先级
    }
}

class T extends Thread { // 自定义线程类
    public void run() {
        while (true) {
            for (int i = 0; i < 100; i++) {
                System.out.println(Thread.currentThread().getName() + " 吃包子 " + i);
            }
            try {
                System.out.println(Thread.currentThread().getName() + " 休眠中 ");
                Thread.sleep(20000);// 20秒
            }catch (Exception e) {
                // 当该线程执行到一个 interrupt 方法时，就会 catch 一个异常，可以加入自己的业务代码
                // 提前中断一个线程
                System.out.println(Thread.currentThread().getName()+ "被 interrupt了");
            }
        }
    }
}