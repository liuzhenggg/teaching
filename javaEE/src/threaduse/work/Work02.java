package threaduse.work;

/**
 * 龟兔赛跑问题
 */
public class Work02 {
    public static void main(String[] args) {
        // 1. 定义 乌龟的线程类 和 兔子的线程类
        // 2. 让乌龟 和兔子 开始跑
        // 3. 谁先跑完就 输出比赛胜利 并终结对方线程
        Turtle turtle = new Turtle();
        Rabbit rabbit = new Rabbit();

        turtle.start();
        turtle.setRabbit(rabbit);
        rabbit.start();

    }
}

// 乌龟线程
class Turtle extends Thread {
    private int distance = 0; // 距离
    private Rabbit rabbit;

    public void run() {
        try {
        // 每循环一次 代表跑了一米
        while (true) {
            if (distance >= 400) {
                System.out.println("乌龟跑完了全程~~~");
                // 判断对方是否还在赛道 --如果是直接宣布比赛胜利 终结对方线程
                if (rabbit.getLoop()){
                    System.out.println("乌龟宣布比赛胜利！");
                    rabbit.setLoop(false);
                }
                break;
            }
            // 开始向前跑
            distance++;
            Thread.sleep(200);
            System.out.println("乌龟开已经奔跑了：" + distance + "米");
        }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public Rabbit getRabbit() {
        return rabbit;
    }

    public void setRabbit(Rabbit rabbit) {
        this.rabbit = rabbit;
    }
}

// 兔子线程
class Rabbit extends Thread {
    private int distance = 0; // 距离
    private boolean loop = true;

    public void run() {
        try {
            // 每循环一次 代表跑了一米
            while (loop) {
                if (distance >= 400) {
                    System.out.println("兔子跑完了全程~~~");
                    // 比赛结束--终结对方线程

                    break;
                }
                if (distance == 200) {
                    System.out.println("兔哥累了，看了看身后的乌龟准备先休息 一分钟  ~~");
                    Thread.sleep(50000);
                    System.out.println("休息完了继续上路~~~");
                }
                // 开始向前跑
                distance++;
                Thread.sleep(100);
                System.out.println("兔子开已经奔跑了：" + distance + "米");
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean getLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }
}