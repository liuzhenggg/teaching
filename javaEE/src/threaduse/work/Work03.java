package threaduse.work;

// 在日常工作生活中，无论哪个行业都会设置一些VIP用户， VIP用户具有超级优先权，在办理业务时， VIP用户具有最大的优先级。
//要求编写一个模拟VIP优先办理业务的程序，在正常的业务办理中，插入一个VIP用户，优先为VIP用户办理业务。通过多线程实现。
public class Work03 {
    public static void main(String[] args) {
        new Normal().start();
    }
}


// 普通用户
class Normal extends Thread {
    public void run() {
        System.out.println("普通用户正在排队 准备办理业务 ~~~~");



        for (int i = 0; i <= 10; i++) {
            try {
                Thread.sleep(100);
                if (i == 5) {
                    try {
                        System.out.println("此时来了个 VIP 用户 进行了插队行为 ~~");
                        Special special = new Special();
                        special.start();
                        special.join();
                    }catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }

            }catch (Exception e) {
                System.out.println(e.getMessage());
            }

            System.out.println("终于排到了，开始办理业务~~~ 步骤: " + i);
        }

    }
}

// VIP
class Special extends Thread {
    public void run() {
        System.out.println("VIP 用户 不要排队 可直接办理业务 ~~~");
        System.out.println("办理完成~~~");
    }
}