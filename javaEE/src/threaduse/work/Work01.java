package threaduse.work;

import java.sql.SQLOutput;
import java.util.Scanner;

/*

 */
public class Work01 {
    public static void main(String[] args) {
        T t = new T();
        t.setName("线程1");
        t.start();

        T2 t2 = new T2();
        t2.setT(t);
        t2.setName("线程2");
        t2.start();
    }
}


class T extends Thread {

    private boolean loop = true;
    public void run() {
        try {
            while (loop) {
                for (int i = 0; i < 10; i++) {
                    Thread.sleep(1000);
                    System.out.println(Thread.currentThread().getName() + "循环打印整数:" + i);
                }
            }
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("线程1 退出");

    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }
}


class T2 extends Thread {
    private T t;

    public void setT(T t) {
        this.t = t;

    }

    public void run() {
        System.out.println(Thread.currentThread().getName() + "从键盘获取输入: ");
        while (true) {
            Scanner sc = new Scanner(System.in);
            String next = sc.next();
            if ("Q".equals(next)) {
                t.setLoop(false);
                break;
            }
        }
        System.out.println("线程2 退出~~~");
    }
}