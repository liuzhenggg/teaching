package threaduse;

public class ThreadExit {
    public static void main(String[] args) throws Exception{
        T t1 = new T();
        t1.start();
        // 主线程 通知结束
        // 主线程休眠
            // 10 秒后通知 t 线程结束
        System.out.println("主线程休眠10秒....0");
        Thread.sleep(10*1000);
        t1.setLoop(false);

    }
}

class T extends Thread {
    // 设置一个控制变量
    private boolean loop = true;

    @Override
    public void run() {
        int count = 0;
        while (loop) {
            try {
                Thread.sleep(50); // 让当前线程休眠50ms
            }catch (Exception e) {
                System.out.println(e.getMessage());
            }
            System.out.println("T 运行中....... " + (count++));

        }
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }
}