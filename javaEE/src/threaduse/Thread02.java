package threaduse;

/**
 * 使用 Runnable 接口创建线程
 */
public class Thread02 {
    public static void main(String[] args) {
        Dog dog = new Dog();
        // dog.run(); // 刚才讲了 不能直接调用这个 run 方法 --- 不是一个线程
        Thread thread = new Thread(dog); // 这里使用了代理模式 -- 我简单模拟下代码
        thread.start();

        // 模拟代码的应用
        Tiger tiger = new Tiger();// 实现了 Runnable
        ThreadProxy proxy = new ThreadProxy(tiger);
        proxy.start();
    }
}

class Dog implements Runnable {

    @Override
    public void run() {
        try {
            int cont = 0;
            while (true) {
                System.out.println("狗子汪汪叫~~~~" + cont++ +" "+  Thread.currentThread().getName());
                Thread.sleep(1000);
                if (cont == 8) {
                    break;
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}

/*
 * 代理模式 的代码模拟 --- 模拟了一个及简的 Thread 类
 */
class ThreadProxy implements Runnable { // 大家可以把Proxy类当做 ThreadProxy
    private Runnable target = null; // 属性，类型是 Runnable
    @Override
    public void run() {
        if (target == null) {
            target.run();// 动态绑定
        }
    }
    public ThreadProxy(Runnable target) {
        this.target = target;
    }
    public void start(){
        start0(); // 这个方法是真正实现多线程的方法 ---- 模拟
    }
    public void start0(){
        run();
    }
}

// 模拟代码的应用
class Animal {}
class Tiger extends Animal implements Runnable {

    @Override
    public void run() {
        System.out.println("老虎嗷嗷叫~~");
    }
}