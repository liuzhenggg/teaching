package threaduse;

/**
 * 演示 通过继承 Thread 类创建线程
 */
public class Thread01 {
    public static void main(String[] args) throws InterruptedException {
        Cat cat = new Cat();
        cat.start(); // 启动线程

        // 三阶段要讲的代码  为什么不直接 调用 run 方法？
        // cat.run(); --- 这样还是相对于主线程在去执行 而不是新开一个线程

        /*  四阶段 --- 简单讲下 start 方法
            （1） private native void start0();

            （2）start0() 是本地方法，是JVM调用，底层是 C/C++ 实现的 【本地的 native 方法】
            真正实现多线程的效果 是 start0() 而不是 run 【run 只是线程执行的任务】
        */

        // 2. 阶段代码 --- 主线程 和 start 线程交替执行
        // 当 main 线程启动一个子线程 Thread-0 主线程不会阻塞，会继续执行
        System.out.println("主线程继续执行" + Thread.currentThread().getName());
        for (int i = 0; i < 10; i++) {
            System.out.println("主线程 i=" + i);
            // 让主线程休眠
            Thread.sleep(1000);
        }
    }
}

/*
    说明：
    1. 当一个类继承了 Thread 类，该类就可以当做线程使用
    2. 我们会重写 Thread 类 实现了 Runnable 接口的 run 方法
 */
class Cat extends Thread {
    int count = 0;
    // 重写 run 方法 写上自己的业务逻辑，这里就是你开的这个线程的执行代码
    @Override
    public void run() {
        try {
            while (true) {
                System.out.println("喵喵喵 ~~~ 小猫在吃鱼" + count++);
                Thread.sleep(1000);

                if (count == 8) {
                    break; // 循环 8 次 退出 --- 这时线程也就退出了~~~~
                }
            }
             // 线程睡眠 1000 毫秒
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


}