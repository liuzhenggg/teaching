package threaduse;

/**
 * 线程插队 练习
 */
public class ThreadMethodExercise {
    public static void main(String[] args) throws InterruptedException {

        for (int i = 1; i <= 10; i++) {
            System.out.println("hi " + i);
            Thread.sleep(1000);

            if (i == 5) {
                // 启动子线程
                T3 t3 = new T3();
                Thread thread = new Thread(t3);
                thread.start();
                thread.join();
            }

        }
        System.out.println("主线程结束....");

    }
}

class T3 implements Runnable {

    public void run() {
        for (int i = 1; i <= 10; i++) {
            System.out.println("hello " + i);
        }
        System.out.println("子线程结束......");

    }
}