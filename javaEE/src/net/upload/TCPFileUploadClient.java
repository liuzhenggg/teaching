package net.upload;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * 文件上传的客户端
 */
public class TCPFileUploadClient {
    public static void main(String[] args) throws IOException {
        // 客户端 连接服务端 6666 得到 Socket 对象
        Socket socket = new Socket(InetAddress.getLocalHost(),6666);

        // 创建读取磁盘文件的输入流
        String filePath = "e:\\风景.jpg";
        // 使用 包装流中的 缓冲字节输入流
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filePath));
        // bytes 就是 filePath 对应的字节数组
        byte[] bytes = StreamUtils.streamToByteArray(bis);

        // 通过 socket 获取到输出流，将 bytes 数据发送给服务端
        BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
        bos.write(bytes); // 将数据 写入到 socket 管道里面
        System.out.println("写入文件成功");
        bis.close();
        socket.shutdownOutput();// 设置写入数据的结束标记

        // === 二阶段 接收 服务端消息的代码
        InputStream is = socket.getInputStream();
        String s = StreamUtils.streamToString(is);
        System.out.println(s);
        // === 二阶段

        // 关闭相关的流
        bos.close();
        socket.close();
    }
}
