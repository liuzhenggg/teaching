package net.upload;

import java.io.*;

/**
 * 工具类
 */
public class StreamUtils {

    /**
     * 功能: 将输入流转换成 byte[],即 可以把文件的内容读入到 byte[]
     */
    public static byte[] streamToByteArray(InputStream inputStream) throws IOException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream(); // 创建输出流对象
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) != -1) {
            bos.write(buffer, 0, length);
        }
        byte[] bytes = bos.toByteArray(); // 全写入到一个字节数组里面
        bos.close();
        return bytes;
    }

    public static String streamToString(InputStream inputStream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder builder = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) { // 当读取到 null 时就代表读取结束
            builder.append(line+"\r\n");
        }
        return builder.toString();
    }
}
