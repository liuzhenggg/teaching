package net.upload;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 文件上传的服务端
 */
public class TCPFileUploadServer {
    public static void main(String[] args) throws IOException {
        // 1. 服务端在本机监听 6666 端口
        ServerSocket serverSocket = new ServerSocket(6666);
        System.out.println("服务端再在6666端口监听.....");
        // 2. 等待连接
        Socket socket = serverSocket.accept();

        // 3. 读取客户端发生的数据 --- 先去把客户端写完
        // 通过 Socket 得到输入流
        BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
        // 传入 流 得到字节数组
        byte[] bytes = StreamUtils.streamToByteArray(bis);
        // 4. 将得到 bytes 数组，写入到指定的路径，就得到了一个文件了
        String destFilePath = "C:\\Users\\Mr.Liu\\Desktop\\codeT\\teaching\\javaEE\\src\\风景2.jpg";
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destFilePath));
        bos.write(bytes); // 写入文件
        bos.close(); // 关闭资源

        // === 向客户端回复 “收到图片” 完成一阶段的功能后在讲这个代码
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        bw.write("收到图片");
        bw.flush();// 把内容刷新到数据通道
        socket.shutdownOutput();// 设置写入结束标记
        // ==== 二阶段代码结束位置

        // 关闭其他资源
        bis.close();
        socket.close();
        serverSocket.close();
    }
}
