package net.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 发送端 B
 */
public class UDPSenderB {
    public static void main(String[] args) throws Exception {
        // 创建 DatagramSocket 对象，准备发送 和 接收数据
            // 发送端将来 也可以接收数据 在 端口 9998
        DatagramSocket socket = new DatagramSocket(9998);
        //2. 将需要发送的数据，封装到 DatagramPacket 对象
        byte[] data = "Hello 明天吃火锅~~".getBytes();
        // 这里上节时要注意看Ip 应该是会变的
        // 说明：封装的 DatagramPacket 对象 data 内容的字节数组， data.length, 主机（IP）, 端口
        DatagramPacket packet = new DatagramPacket(data,data.length, InetAddress.getByName("192.168.1.3"), 9999);

        socket.send(packet);

        //==== 2 阶段 接收从A 端回复的信息
        //(1)   构建一个 DatagramPacket 对象，准备接收数据
        byte[] buf = new byte[1024];
        packet = new DatagramPacket(buf, buf.length);
        //(2)   调用 接收方法，将通过网络传输的 DatagramPacket 对象 填充到 packet 对象
        socket.receive(packet);
        //(3) 拆包进行 输出
        int len = packet.getLength();
        data = packet.getData();
        String s = new String(data,0,len);
        System.out.println(s);
        // ===== 2 阶段 同学们先写

        // 关闭资源
        socket.close();
        System.out.println("B 端退出");
    }
}
