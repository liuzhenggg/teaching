package net.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * 接收端 A
 */
public class UDPReceiverA {
    public static void main(String[] args) throws Exception {
        // 1. 创建一个 DatagramSocket 对象，准备在 9999接收数据
        DatagramSocket socket = new DatagramSocket(9999);
        // 2. 构建一个 DatagramSocket 对象，准备接收数据
        //      在前面讲解 UDP 协议时，我说过 一个数据包最大 64 K
        byte[] buf = new byte[1024];
        DatagramPacket packet = new DatagramPacket(buf, buf.length);
        // 3. 调用 接收方法，将通过网络传输的 DatagramPacket 对象 填充到 packet 对象
            // 提示：当有数据包发送到 本机的 9999 端口时就会接收到数据
            // 如果没有 数据包发送到 本机的 9999 端口就会阻塞等待
        System.out.println("接收端A 等待接收数据...");
        socket.receive(packet);
        // 4. 可以把 packet 进行拆包，取出数据，并显示
        int len = packet.getLength();// 实际接收到的数据字节长度
        byte[] data = packet.getData(); // 接收到数据
        String s = new String(data, 0, len); // 将字符数组转换为字符串
        System.out.println(s);

        // ==== 二阶段 先让同学们先写 你在讲
        data = "好的，明天见".getBytes();
        packet = new DatagramPacket(data,data.length, InetAddress.getByName("192.168.1.3"), 9998);
        // 发送
        socket.send(packet);

        // 5. 关闭资源
        socket.close();
        System.out.println("A端 关闭");
    }
}
