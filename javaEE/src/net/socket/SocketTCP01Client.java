package net.socket;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * 客户端
 * 发送信息到服务端
 */
public class SocketTCP01Client {

    public static void main(String[] args) throws IOException {
        // 思路
        // 1. 连接服务端 (ip，端口)  --- 可以换成 其他需要连接的IP 地址
        // 解读： 我要连接 InetAddress.getLocalHost() 这台主机的 9999 端口
        Socket socket = new Socket(InetAddress.getLocalHost(), 9999);
        System.out.println("客户端 socket返回=" + socket.getClass());
        // 2.连接上后，生成Socket,通过 Socket.get
        OutputStream out =  socket.getOutputStream();
        // 3.通过输出流，写入数据到数据通道
        out.write("hello".getBytes());
        // 4. 关闭流对象 和 socket,必须关闭
        out.close();
        socket.close();
        System.out.println("客户端退出....");

    }
}
