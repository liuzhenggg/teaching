package net;

import java.net.InetAddress;

/**
 *
 * InetAddress 的使用
 *
 */
public class API_ {
    public static void main(String[] args) throws Exception {
        InetAddress localHost = InetAddress.getLocalHost();
        // 机器名 和 ip地址
        System.out.println(localHost);

        // 2. 根据指定主机名 获取 InetAddress 对象
        InetAddress host1 = InetAddress.getByName("LAPTOP-VHKM4MRG");
        System.out.println(host1);

        //3. 根据域名返回 InetAddress 对象 比如 www.baidu.com
        InetAddress host2 = InetAddress.getByName("www.baidu.com");
        System.out.println(host2);

        //4. 通过 InetAddress 对象，获取对应的地址
        String hostAddress = host2.getHostAddress();
        System.out.println(hostAddress);

        //5. 通过 InetAddress 对象，获取对应的主机名
        String hostName = host2.getHostName();
        System.out.println(hostName);
    }
}
