package net.work;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/**
 * UDP 发送端 B
 */
@SuppressWarnings({"ALL"})
public class Work02SenderB {
    public static void main(String[] args) throws Exception {
        // 1. 创建 DatagramSocket 对象，准备在 9998 端口接收数据
        DatagramSocket socket = new DatagramSocket(9998);

        // 2. 将需要发送的数据 封装到 DatagramPacket 对象
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入你的问题：");
        String question = sc.next();
        byte[] data = question.getBytes();

        // 打包
        DatagramPacket packet = new DatagramPacket(data,data.length, InetAddress.getLocalHost(), 8888);
        // 发送
        socket.send(packet);

        // 接收从A 端回复的信息
        //(1)   构建一个 DatagramPacket 对象，准备接收数据
        byte[] buf = new byte[1024];
        packet = new DatagramPacket(buf, buf.length);
        //(2)   调用 接收方法，将通过网络传输的 DatagramPacket 对象 填充到 packet 对象
        socket.receive(packet);
        //(3) 拆包进行 输出
        int len = packet.getLength();
        data = packet.getData();
        String s = new String(data,0,len);
        System.out.println(s);
        // 关闭资源
        socket.close();
        System.out.println("B 端退出");

    }
}
