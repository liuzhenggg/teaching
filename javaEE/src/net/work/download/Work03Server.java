package net.work.download;

import net.upload.StreamUtils;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 先写 文件下载的服务端
 */
public class Work03Server {
    public static void main(String[] args) throws IOException {
        // 监听
        ServerSocket serverSocket = new ServerSocket(9999);
        // 等待连接
        System.out.println("服务端在 9999 端口监听.....");
        Socket socket = serverSocket.accept();
        // 读取 文件名
        InputStream inputStream = socket.getInputStream();
        byte[] b = new byte[1024];
        int len = 0;
        String downLoadFileName = "";
        while ((len = inputStream.read(b)) != -1) {
            // 获取文件名
            downLoadFileName += new String(b, 0, len);
        }
        System.out.println("客户端 希望下载的文件名= " + downLoadFileName);
        // 服务器上有两个文件 一个 风景.jpg 一个 无名.jpg
        String reFileName = "";
        if (downLoadFileName.equals("风景")) {
            reFileName = "e:\\风景.jpg";
        }else {
            reFileName = "e:\\无名.jpg";
        }
        // 4. 创建一个 输入流，读取文件
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(reFileName));
        // 5. 使用工具 得到了一个 文件 的 字节数组
        byte[] bytes = StreamUtils.streamToByteArray(bis);
        // 6. 得到 Socket 关联的输出流
        BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
        bos.write(bytes); // 写入数据通道
        // 结束标记
        socket.shutdownOutput();
        // 关闭 相关资源
        bis.close();
        bos.close();
        socket.close();
        serverSocket.close();
        System.out.println("服务端退出>>>>>>>>");
    }


}
