package net.work;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

@SuppressWarnings({"all"})
public class WorkServer01 {
    public static void main(String[] args) throws IOException {
       ServerSocket serverSocket = new ServerSocket(9999);
        System.out.println("服务端，在9999端口监听，等待连接..");
        Socket socket = serverSocket.accept();
        InputStream in = socket.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String line = br.readLine();
        String answer = "";
        if ("name".equals(line)) {
            answer = "俺是刘政";
        }else if ("hobby".equals(line)) {
            answer = "唱跳rap篮球";
        }else {
            answer = "听不懂思密达";
        }

        OutputStream out = socket.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
        bw.write(answer);
        bw.newLine();// 插入一个换行符，表示回复内容结束
        bw.flush();

        socket.close();
        serverSocket.close();
        bw.close();
        br.close();
    }
}
