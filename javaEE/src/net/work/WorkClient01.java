package net.work;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

@SuppressWarnings({"all"})
public class WorkClient01 {

    public static void main(String[] args) throws IOException {
        // 思路
        // 1. 连接服务端 (ip，端口)  --- 可以换成 其他需要连接的IP 地址
        // 解读： 我要连接 InetAddress.getLocalHost() 这台主机的 9999 端口
        Socket socket = new Socket(InetAddress.getLocalHost(), 9999);
        // 2.连接上后，生成Socket,通过 Socket.get
        OutputStream out =  socket.getOutputStream();
        // 3. 使用转换流将上面的流转换为 字符流
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(out));
        // 键盘输入问题
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入你的问题");
        String question = scanner.next();

        bufferedWriter.write(question);
        bufferedWriter.newLine();// 插入一个换行符 表示写入的内容结束
        bufferedWriter.flush();

        // 4. 获取和 socket 关联的输入流，读取数据（字符），并显示
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String s = bufferedReader.readLine();
        System.out.println(s);

        // 5. 关闭流对象 和 socket
        bufferedReader.close();
        bufferedWriter.close();
        socket.close();
        System.out.println("客户端退出......");

    }
}
