package fileter;


import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

/**
 * 此过滤器的作用是记录 请求的历史 将日志打印到控制台
 * 使用过滤器的三个步骤
 * 1. 实现 Filter 接口
 * 2. 重写过滤方法
 * 3. 配置过滤器
 *          通过 xml
 *          通过 注解
 */
@WebFilter(
        filterName = "loggingFilter",
        initParams = {@WebInitParam(name="dateTimePattern",value="yyyy-MM-dd HH:mm:ss")},
        urlPatterns = {"/ServletArea1","*.html"},
        servletNames = {"servletBName"}
)
public class LoggingFilter implements Filter {


    /**
     * 过滤请求 和 响应的方法
     *     1.请求到达目标资源之前，先经过该方法
     *     2.该方法有能力控制请求是否继续向后到达目标资源 可以在该方法内直接向客户端做出响应处理
     *     3.请求到达目标资源后，响应之前，还会经过该方法【门卫】
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        /*
         * 1. 请求到达目标资源之前的功能代码
         *          判断是否登录
         *          校验是否有对应权限
         *          ..............
         * 2. 放行代码
         *
         * 3.响应之前 HttpServletResponse 转换成响应报文之前的代码
         */
        System.out.println("LoggingFilter before FilterChain.doFilter ");

        // 放行  注意这里不是递归 过滤器也可以不止一个的 这个是交给下一个过滤器
        filterChain.doFilter(servletRequest, servletResponse);


        System.out.println("LoggingFilter after FilterChain.doFilter ");

        /* debug 完了 再写下面这些代码
          // 参数父转子
        HttpServletRequest request =(HttpServletRequest)  servletRequest;
        HttpServletResponse  response =(HttpServletResponse)  servletResponse;
        // 拼接日志文本
        String requestURI = request.getRequestURI();
        String time = dateFormat.format(new Date());
        String beforeLogging =requestURI+"在"+time+"被请求了";
        // 打印日志
        System.out.println(beforeLogging);
        // 获取系统时间
        long t1 = System.currentTimeMillis();
        // 放行请求
        filterChain.doFilter(request,response);
        // 获取系统时间
        long t2 = System.currentTimeMillis();
        //  拼接日志文本
        String afterLogging =requestURI+"在"+time+"的请求耗时:"+(t2-t1)+"毫秒";
        // 打印日志
        System.out.println(afterLogging);
         */
    }
}
