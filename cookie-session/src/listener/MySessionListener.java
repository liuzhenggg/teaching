package listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class MySessionListener implements HttpSessionListener, HttpSessionAttributeListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        // 注意 任何一个 session 域对象创建后都会触发该方法
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        // 任何一个 session 过期销毁 都会调用该方 法
    }

    public void attributeAdded(HttpSessionBindingEvent se) {
        // 任何一个.......
    }
}
