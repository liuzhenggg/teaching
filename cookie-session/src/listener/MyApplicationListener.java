package listener;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * ServletContextListener 对象的创建和销毁
 * ServletContextAttributeListener 域对象中的数值改变
 */
@WebListener
public class MyApplicationListener implements ServletContextListener, ServletContextAttributeListener {
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("contextInitialized 结合生命周期 当应用域对象被初始化后就会执行这个方法" );
    }

    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("销毁~~~~");
    }

    // 添加
    public void attributeAdded(ServletContextAttributeEvent sce) {
         String key = sce.getName();
         Object value = sce.getValue();
        System.out.println("应用域中增加了：" + key + " : " + value);
    }

    // 删除
    public void attributeRemoved(ServletContextAttributeEvent sce) {
        String key = sce.getName();
        Object value = sce.getValue();
        System.out.println("应用域中删除了：" + key + " : " + value);
    }

    // 修改
    public void attributeReplaced(ServletContextAttributeEvent sce) {

        String key = sce.getName();
        Object value = sce.getValue();
        System.out.println("应用域中修改了：" + key + " : " + value);
    }

}
