package session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 接收参数
        String userName = req.getParameter("userName");

        // 获取 session 对象
        HttpSession session = req.getSession();
        // 判断请求中有咩有一个 特殊的 cookie JSESSIONID 值 *****   【有图】
            // 1. 有
                // 根据 JSESSIONID 找 对应的 session 对象
                    // 找到了
                        // 返回
                    // 没找到 --- 有最大存活时间
                        // 创建 一个新的 session 返回，并且向 resp 对象中存储一个 JSESSIONID 的cookie
            // 2. 没有
                // 创建一个新的 session 返回，并向 resp 对象中存放一个 JSESSIONID 的 cookie
        System.out.println(session.getId());
        System.out.println(session.isNew());

        // 将 userName 放入 session
        session.setAttribute("userName", userName);

        // 客户端响应信息
        resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().write("成功");

    }
}
