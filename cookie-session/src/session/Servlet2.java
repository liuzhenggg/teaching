package session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 获取 在 Servlet1 中 对 session 对象存储的信息
 */
@WebServlet("/Servlet2")
public class Servlet2 extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 获取 Session 对象
        HttpSession session = req.getSession();

        System.out.println("Servlet2：" + session.getId());
        System.out.println(session.isNew());

        // 读取 session 中存储的用户名
        String username = (String) session.getAttribute("userName");
        System.out.println("Servlet2 从 Session 中获取的用户名是: " + username );


    }
}
