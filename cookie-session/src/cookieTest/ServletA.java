package cookieTest;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 创建 Cookie
 * cooke 分两种 一种是默认 会话级Cookie 【浏览器关闭后清除 快捷键 Ctrl + shift + delete】
 */
@WebServlet("/servletA")
public class ServletA extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 创建 cookie
        Cookie cookie = new Cookie("keyA", "valueA");
        Cookie cookie2 = new Cookie("keyB", "valueB");

        // 可以通过 cookie 的 API
        // 持久化 保存 cookie0
        cookie.setMaxAge(60*5); // 单位默认是秒 设置持久化 5分钟

        // 默认浏览器 拿到 cookie 后 请求任意 url 都会 携带 cookie 我们后端可以设置提交路径
        cookie.setPath("/servletB");

        // 将 cookie 放到 resp 对象
        resp.addCookie(cookie);
        resp.addCookie(cookie2);

    }
}
