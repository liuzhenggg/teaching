package service;

import common.Message;
import common.MessageType;
import common.User;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

public class QQService {

    private ServerSocket ss = null;

    private static HashMap<String, User> validUser = new HashMap<String, User>();
    // 初始化用户数据
    static {
        validUser.put("孙悟空", new User("孙悟空", "123456"));
        validUser.put("李白", new User("李白", "root"));
        validUser.put("迪迦", new User("迪迦", "光"));
        validUser.put("100", new User("100", "root"));
        validUser.put("admin", new User("admin", "admin"));
        validUser.put("1", new User("1", "1"));
    }

    private boolean checkUser(String userId, String password) {
        User user = validUser.get(userId);
        if (user == null) {
            System.out.println("该用户不存在===");
            return false;
        }
        if (!user.getPassword().equals(password)) {
            System.out.println("密码错误!!!!");
            return false;
        }
        return true;
    }

    public QQService() {
        try{
            System.out.println("服务器在 9999 端口监听~~~");
            new Thread(new SendNewsToAllService()).start();
            ss = new ServerSocket(9999);
            // 这里是一直在监听
            while(true){
                // 当和某个客户端连接后，会继续监听，因此while
                // 如果没有客户端连接则会阻塞到这里
                Socket socket = ss.accept();
                // 拿到 socket 关联的对象输入流
                ObjectInputStream ois =  new ObjectInputStream(socket.getInputStream());
                // 读取对象信息
                User user = (User) ois.readObject();
                // 通过对象输出流 回复给客户端
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                // 理论上是要有数据库的 -- 但现在还没有
                // 验证
                Message message = new Message();
                if (checkUser(user.getUserId(), user.getPassword())) {
                // if (user.getUserId().equals("100") && user.getPassword().equals("root")){
                    // 创建一个 Msg 对象回复客户端
                    message.setMesType(MessageType.MESSAGE_LOGIN_SUCCEED);
                    oos.writeObject(message);

                    // 登录成功，创建一个线程，和客户端保持通信，该线程需要持有 socket
                    ServerConnectClientThread thread = new ServerConnectClientThread(socket, user.getUserId());
                    // 启动线程
                    thread.start();
                    // 把该线程对象放到一个集合中进行管理
                    ManageClientThreads.addClientThread(user.getUserId(), thread);

                }else {// 登录失败
                    System.out.println("用户 id= " + user.getUserId() + "pwd= " + user.getPassword() + "验证失败！！");
                    message.setMesType(MessageType.MESSAGE_LOGIN_FAIL);
                    oos.writeObject(message);
                    // 失败的情况 关闭 socket
                    socket.close();
                }
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            // 这里是最后执行的代码 如果服务端退出 while 则关闭资源
            try {
                ss.close();
            }catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }

    // 启动测试
    public static void main(String[] args) {
        new QQService();
    }
}
