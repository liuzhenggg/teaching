package service;

import common.Message;
import common.MessageType;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;

/**
 * 该类的一个对象 和某个客户端保持通信
 */
public class ServerConnectClientThread extends Thread {
    private Socket socket;

    private String userId;// 记录的和谁连接的

    public ServerConnectClientThread(Socket socket, String userId) {
        this.socket = socket;
        this.userId = userId;
    }

    public Socket getSocket() {
        return socket;
    }

    // 这里线程处于一个运行的状态，可以发送/接收消息
    public void run() {
        while (true) { // 目前是要一直保持连接的
            try {
                System.out.println("服务端和客户端 "+ userId +"保持通信，读取数据~~~");
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                Message message = (Message) ois.readObject();

                // 先不用 后面我们会使用 message
                // 返回用户列表的逻辑
                if (message.getMesType().equals(MessageType.MESSAGE_GET_ONLINE_FRIEND)) {
                    System.out.println(message.getSender() + " 要在线用户列表");
                    String onlineUser = ManageClientThreads.getOnlineUser();
                    // 返回 msg 对象
                    Message m2 = new Message();
                    m2.setMesType(MessageType.MESSAGE_RET_ONLINE_FRIEND);
                    m2.setContent(onlineUser);
                    m2.setGetter(message.getSender());

                    // 回写数据
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(m2);
                }else if(message.getMesType().equals(MessageType.MESSAGE_CLIENT_EXIT)){
                    // 退出登录
                    System.out.println(message.getSender() + "退出");
                    ManageClientThreads.removeClientThread(message.getSender());
                    socket.close();
                    // 退出循环
                    break;
                }else if (message.getMesType().equals(MessageType.MESSAGE_COMM_MES)){
                    // 服务器这里就转发下就ok了
                    ServerConnectClientThread getter = ManageClientThreads.getClientThread(message.getGetter());
                    // 得到 对应的 socket 的对象输出流 将 message 对象转发给指定的客户端
                    ObjectOutputStream oos = new ObjectOutputStream(getter.getSocket().getOutputStream());
                    // 转发
                    oos.writeObject(message);

                }else if (message.getMesType().equals(MessageType.MESSAGE_TO_ALL_MES)){
                    // 需要遍历 管理线程的集合, 把所有的线程的 socket 得到，然后把 message 进行转发即可
                HashMap<String, ServerConnectClientThread> hm = ManageClientThreads.getHm();

                // 先获取 key 为一个集合 然后 得到该集合的迭代器
                Iterator<String> it = hm.keySet().iterator();
                while (it.hasNext()) {
                    // 取出在线用户ID
                    String onLineUserId = it.next();

                    // 如果是自己就不用发了
                    if (!onLineUserId.equals(message.getSender())) {
                        //  拿到在线用户线程 中的 socket 对象
                        //  有了socket 就可以写入参数了
                        ObjectOutputStream oos = new ObjectOutputStream(hm.get(onLineUserId).getSocket().getOutputStream());
                        oos.writeObject(message);
                    }
                }

                } else if (message.getMesType().equals(MessageType.MESSAGE_FILE_MES)) {
                    // 发送文件的业务逻辑
                    // 根据getter Id 获得到对应的线程，将 message 对象转发
                    ServerConnectClientThread thread = ManageClientThreads.getClientThread(message.getGetter());
                    ObjectOutputStream oos = new ObjectOutputStream(thread.getSocket().getOutputStream());
                    // 转发
                    oos.writeObject(message);
                    // 之后去接收者的 客户端写接收的代码

                } else {
                    System.out.println("其他业务。。暂不处理");
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }
    }

}
