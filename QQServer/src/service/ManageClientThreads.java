package service;

import java.util.HashMap;
import java.util.Iterator;

/**
 * 服务端
 * 用于管理和客户端通讯的线程
 */
public class ManageClientThreads {
    private static HashMap<String,ServerConnectClientThread> hm = new HashMap<>();

    // 添加线程对象到 hm 集合  --- 为了进行统一的管理
    public static void addClientThread(String userId,ServerConnectClientThread ct){
        hm.put(userId,ct);
    }

    // 根据 userId 返回 ServerConnectClientThread 线程
    public static ServerConnectClientThread getClientThread(String userId) {
        return hm.get(userId);
    }

    // 这里返回当前登录的用户列表 -- 其实就是线程的 id
    public static String getOnlineUser() {
         Iterator<String> iterator = hm.keySet().iterator();
         String onlineUser = "";
         while(iterator.hasNext()){
             onlineUser += iterator.next() + " ";
         }
         return onlineUser;
    }

    // 退出登录 移除线程
    public static void removeClientThread(String userId) {
        hm.remove(userId);
    }


    // 返回所有线程集合
    public static HashMap<String, ServerConnectClientThread> getHm() {
        return hm;
    }
}
